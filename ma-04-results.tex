\newcommand{\src}[1]{Created by \mbox{\texttt{#1()}}.}
\chapter{Results}\label{ch:results}
\vspace{-5mm}

This chapter investigates the nano--tetrapods’ magnetic properties
by exploring the response 
to an external magnetic field.
The investigation explores symmetries
and questions reproducibility
to answer where and how magnetic noise
emerges.
First, the nano--tetrapods’ ferromagnetic fingerprint
is observed through hysteresis loops.
The structures’ comparison extends throughout
selected angles $\theta$ of the external magnetic field $H_{ext}$.
As expected, repetitions with identical initial conditions
question reproducibility by yielding different fingerprints. 
To solve this puzzle, the measurements’ noise is analyzed 
in the frequency space with the signal--analyzer.
This analysis hints towards regions of interest in the hysteresis.
Additionally, the data acquisition functionality of the SR830
allows an additional analysis of the temporal signal in these regions of interest.
This additional analysis unveils noise in 
metastable states inside the hysteresis.
Finally, the signal--analyzer’s measurements are reproduced using
the novel SR830DAQ functionality.

All displayed plots were created using \texttt{ana}’s test cases available via the supplemental information (see Appendix~\ref{ch:supplement}).
Interested readers are welcome to examine the source code and data.
For this purpose, every caption includes the function name 
used to create the corresponding plot.
Some legends and titles also include the measurement number,
granting individual data exploration and employment of personal analysis techniques.

\section{Magnetic Hysteresis Loops}

Plusses (left) and Crosses (right) have a characteristic, angle--sensitive hysteresis loops (see  Fig.~\ref{fig:hloop-45} and~\ref{fig:hloop-90}).  
Plusses and Crosses display individual hysteresis loops at almost all angles.
The average magnetic stray--field through the active area of the Hall sensor $\langle B_z \rangle$
is recorded during an up--and down--sweep of the external magnet.
Therefore, the shown values for the magnetic stray--field $\langle B_z \rangle$ should be interpreted as relative values.

\figtop{\textwidth}{hysteresis_45}{fig:hloop-45}{Hysteresis Loops ($\theta = \pm 45^{\circ}$)}{Magnetic hysteresis curves measured as %
	magnetic stray--field $\langle B_z \rangle$ %
	during up--and down--sweep %
	of the external field $H_{ext}$ %
	at $T = 30\,$K. %
	The measured stray--field $\langle B_z \rangle$ is shown as a difference from the negative saturation. %
	After setting the angle $\theta$ to %
	$+45^{\circ}$ (upper half) or %
	$-45^{\circ}$ (lower half), %
	the Plusses (left) and Crosses (right) are measured %
	subsequently using the gradiometry technique.\index{Micro-Hall magnetometry!Gradiometry} %
	\src{test\_plot\_compare\_hyst}}

\figtop{\textwidth}{hysteresis_90}{fig:hloop-90}{Hysteresis Loops ($\theta = \pm 90^{\circ}$)}{Magnetic hysteresis curves for an external field applied parallel to the sensor’s surface. Measured Plusses and Crosses simultaneously with the parallel technique.\index{Micro-Hall magnetometry!Parallel} \src{test\_plot\_compare\_hyst\_par}}

The Plusses and Crosses consist of
multiple rotational and mirror symmetries
(see Figure~\ref{fig:nanostructures}).
Presumed inter--element dipolar coupling between single tetrapods
may increment due to these symmetries.
This dipolar coupling is expected to be
stronger between the Plusses’ tetrapods,
as their arms link horizontally instead of diagonally,
decreasing the interaction distance.
Such magnetically coupled nanostructures
may exhibit a complex switching behavior 
resulting in individually shaped hysteresis loops
depending on various parameters.

\subsection{Angular Comparison}\label{sec:res-hyst}

The hysteresis loops 
display a sensitive behavior
concerning the 
external field’s angular interaction on
the nano--tetrapods’ shape (Plusses or Crosses). 
First, at an angle of $\theta = \pm 45^{\circ}$ 
(see Fig.~\ref{fig:hloop-45}),
the external field acts nearly parallel to the $x$--$z$--plane of at least two arms (compared with Figure~\ref{fig:3d-sensor}).
The gradiometry technique 
applied here allows susceptible measurements,
despite a large signal originating from the external field.
These sensitivities offer several exciting insights.

All four hysteresis loops appear like smooth magnetization curves in most regions, 
except after passing the remanence on the path towards saturation. 
Within the first $50\,\mathrm{mT}$ after the 
externally forced
magnetic polarisation’s reversal,
many spikes and steps occur in all hysteresis loops.
The spikes develop very soon near the remanence.
Later, between 50 to $100\,\mathrm{mT}$, many separate small steps appear,
indicating single switching processes.
The spikes develop a stronger behavior at more extreme angles $\theta = \pm 90^{\circ}$ (see Fig.~\ref{fig:hloop-90}).

\figtop{\textwidth}{hloops_plusses}{fig:hloop-plusses}{Repeated Hysteresis (Plusses)}{Four subsequent repetitions of magnetic field sweeps %
	in parallel configuration ($90^\circ$) %
	with identical initial experimental parameters. %
	Up--and down--sweeps are highlighted %
	in light and dark colors, respectively. %
	Insets display a zoomed--in view of color--coded regions. %
	The difference plot shows %
	a significant variation between the measurements. %
	\src{test\_plot\_compare\_multi\_hyst\_plusses}}

\figtop{\textwidth}{hloops_crosses}{fig:hloop-crosses}{Repeated Hysteresis (Crosses)}{Same repetitions as in Figure~\ref{fig:hloop-plusses}. \src{test\_plot\_compare\_multi\_hyst\_crosses}}

However, for pair--wise particular angles, the loops are quite similar,
for example, at $\pm 45^{\circ}$ or $\pm 90^{\circ}$.
Notably, the particular angles of $\pm 90^{\circ}$ 
open the window of opportunity 
to scrutinize the signal of the nano--tetrapods on top of the sensor. 
Generally, 
these susceptible signals are hidden 
beneath a superposed large background signal 
of the external field,
as explained previously (see Section~\ref{sec:gradiometry}, Page~\pageref{par:experiment}).
Nevertheless,
by measuring the particular angles of $\pm 90^{\circ}$,
one can filter out the superposed signal without the gradiometry technique. 

The Plusses’ and Crosses’ hysteresis loops develop unique characteristics
at these particular angles of $\theta = \pm 90^{\circ}$
without distinctly altering the general description above. 
A closer inspection of Figure~\ref{fig:hloop-90} shows a remarkable similarity between plus and minus $90^{\circ}$.
The contrasting curvatures indicate remnants of the background signal.

Micromagnetic simulations performed by Prof. Michael Huth (Goethe University Frankfurt)
and macro--spin simulations executed by Prof. Christian Schröder (Bielefeld University of Applied Sciences)
revealed details about the magnetization distribution
at several states inside the hysteresis loop.
The micromagnetic simulations
suggest nucleation and propagation of vortex domain wall--like structures near the remanence
\cite{AlMamoori.2020}.
These simulations further indicate that the hysteresis loop’s shape is mainly influenced by the nano--tetrapods’ anisotropy towards the external field (Plusses or Crosses).
Those inter--element dipolar interactions mentioned earlier
may affect the shape of the hysteresis loop
in a subordinate role.
Final simulations and discussions
are still in progress.

To briefly summarize, the hysteresis loops are sensitive 
to the nano--tetrapods’ orientation (Plusses or Crosses)
and the external magnetic field’s applied angle $\theta$, 
except for pair--wise particular angles.
Near the remanence, simulations suggest
vortex--like magnetization states,
represented by spikes in the hysteresis loop.
Still, a relevant question remains. 
Are these results reproducible?

\subsection{Repeated Hysteresis Loops}\label{sec:res-rep}

Figures~\ref{fig:hloop-plusses} and~\ref{fig:hloop-crosses}
show subsequently measured hysteresis loops
of the Plusses and Crosses, respectively.
Zoomed--in color--coded insets
expose a more detailed view of
selected highlighted areas.
These insets reveal a 
remarkable similarity between the measurements 
in regions without observable switching processes.
As a side--note, Figure~\ref{fig:hloop-crosses} contains some peaks with 
a seemingly approaching infinite gradient.
These artefacts result from software bugs in EVE during the measurement,
causing missing characters in the saved values.
This problem only occurred once and could be solved.
The data, however, were left untouched
for integrity reasons and
did not influence any results.

The general features of hysteresis are robustly reproducible (see Fig.~\ref{fig:hloop-plusses} and~\ref{fig:hloop-crosses}).
Nevertheless, a few details are not reproducible: 
the spikes that occur after passing
the remanence on the path towards saturation 
(see Fig.~\ref{fig:hloop-plusses}b-c and Fig.~\ref{fig:hloop-crosses}a). 
When the external field drives 
domain walls through the nanostructures
with characteristic pinning potentials,
complex spin interactions occur.
For such a quantum--mechanical process, 
the uncertainty principle may impose 
particular reproducibility restrictions.
These restrictions only allow a stochastic description of the process.

The investigated nano--tetrapods’ hysteresis loops
are not uniquely reproducible.
The magnetization curves 
of subsequently repeated hysteresis loops 
follow several nearly equivalent
paths in the phase space
that differ between cycles.
This spread $\delta V_H = V_H - \langle V_H \rangle$ from the long--term mean $\langle V_H \rangle$
can be observed 
through a statistical ensemble
of accumulated measurements.
In the saturated (outer) areas of 
Figure~\ref{fig:hloop-plusses} and~\ref{fig:hloop-crosses},
the repeated curves start and end in
slightly different states.
This behavior may result from thermal instabilities 
or capacitive effects in the experimental setup
during the measurements,
preventing the exact repetition of identical states.
Nevertheless, the small ensemble
of the four measurements shown here
already identifies positions 
with considerable fluctuations.
These fluctuations depend on the amplitude and the temporal gradient (up--or down--sweep) of the external magnetic field $H_{ext}$.
This is the primary motivation to investigate
these fluctuations for
desirable, beneficial insights 
into dynamic processes.

\section{Magnetic Flux Noise (MFN)}
The noise (still defined as the spread $\delta V_H$ from the mean) 
comprises several electric and magnetic components.
The noise contributions originating from the detected magnetic flux
are called magnetic flux noise (MFN). 
Unsurprisingly, there are more efficient ways to measure MFN than the one outlined before.


\subsection{Signal--Analyzer (SR785)}\label{sec:res-sa}\index{EVE!SR785}

While noise is typically examined under static conditions,
our approach aims to investigate magnetization dynamics.
The noise of such dynamic processes is more straightforward to measure
by examining 
the magnetic response to an altering external field.
Such an approach has been used by Bertotti 
to study magnetic Barkhausen noise
\cite{Alessandro.1990,Alessandro.1990b,Alessandro.1988,Bertotti.1981}.
His investigations exposed a $1/f^2$ behavior of the PSD
\cite[Ch. 9.3]{Bertotti.2008}.

A state--of--the--art noise measuring technique is using a signal--analyzer. 
The signal--analyzer measures an electric signal and 
outputs the averaged Fourier transformation. 
The FFT over the entire sensor signal during a field sweep 
indicates the MFN’s power--spectral density (PSD) in my experiment.
The PSD is obtained during a field sweep and averaged over multiple bins.
In this process, the signal’s specific values are insignificant,
as the fluctuations are filtered out.

\begin{figure}[p!] \centering
	\begin{subfigure}{.83\linewidth}
		\includegraphics[width=\linewidth]{sr785-fast}
	\end{subfigure}
	\begin{subfigure}{.83\linewidth}
		\includegraphics[width=\linewidth]{sr785-first}
	\end{subfigure}
	\caption[Noise PSD during a field sweep]{\textbf{Noise PSD during field sweep.}\label{fig:sr785-ff} %
		Signal--analyzer (SR785) measurements of the MFN’s PSD during an external field sweep. %
		A dashed line represents $1/f^2$ behavior. %
		Shown are the Hall signal’s PSDs during %
		\textbf{a)}~field sweeps outside and covering the Hysteresis loop ($T = 30\,$K), and %
		\textbf{b)}~multiple field sweeps of the Plusses after negative saturation with different start and end field positions ($T = 5\,$K). %
		Inset shows fitted noise amplitudes $S_{V_H} (f=1\,\mathrm{Hz})$ and slope $\alpha$ %
		from a linear regression between $20\,\mathrm{mHz} < f < 200\,\mathrm{mHz}$.
	\src{test\_sa\_sweeps}}
\end{figure}

Figure~\ref{fig:sr785-ff} shows the MFN’s PSD for different field sweeps.
For sweeps at large fields (blue and green lines), 
only frequency--independent 
thermal background noise is observable.
However, when sweeping inside the hysteresis,
the nanostructures’ magnetic signal 
exclusively exhibits a $S_V \sim 1/f^2$ behavior.
In contrast,
the PSD of an empty Hall bar (light red)
does not share this characteristic behavior.
This is a remarkable observation,
suggesting that the observed MFN indeed originates 
in the magnetization dynamics
of the CoFe nano--tetrapods.
The MFN spectra display no significant change 
considering the range of the sweep, 
as long as the field sweep covers the inside of the hysteresis.

Except for Figure~\ref{fig:sr785-ff}b, all shown measurements up to here were measured at a temperature of $30\,$K.
Figure~\ref{fig:sr785-temp} shows the thermal influence on the MFN’s PSD.
In this measurement, the nano--tetrapods are negatively saturated
before measuring the signal during the up--sweep
between $-25\,$mT and $+25\,$mT.
There is no significant difference in the noise in Figure~\ref{fig:sr785-temp}.
Therefore,
all following measurements have been conducted at $T = 15\,$K.

\figtop{.99\textwidth}{sr785-temp}{fig:sr785-temp}{Comparison of different temperatures (Plusses)}{Multiple noise spectra of the Plusses’ magnetic signal during the up--sweep between $-25\,$mT and $+25\,$mT. Measurements were taken at various temperatures between $5\,$K and $30\,$K (see legend). Insets display resulting amplitude $S_V (f=1\,\mathrm{Hz})$ and slope $\alpha$ from a linear regression between $20\,\mathrm{mHz} < f < 700\,\mathrm{mHz}$. \src{test\_sa\_temp}}

Nevertheless, the signal--analyzer can only provide highly aggregated results.
It can output neither any time series
nor allows it access to intermediate results.
By now, we identified the position inside the hysteresis loop 
where magnetic noise occurs.
In a noise--prone region of this two--dimensional phase space, 
the MFN’s temporal development is scrutinized next.

\subsection{Lock--In Data Acquisition (SR830DAQ)}\label{sec:res-sr}\index{EVE!SR830DAQ}

\figdouble{.48\textwidth}{hist446_1}{m446: Down--sweep}
{.4925\textwidth}{hist447_2}{m447: Up--sweep}
{fig:hist}{Time--signal’s KDE at various field positions inside the hysteresis (Plusses)}{%
	Multiple subsequent measurements %
	taken for $2048\,$s %
	during an interrupted down--/up--sweep %
	at specific field positions %
	annotated left. %
	The external field is applied in an angle $\theta = 45^{\circ}$. %
	Shown is the signal’s KDE after subtraction of the mean value, %
	representing the signal’s distribution $\Delta V_H$ around the mean value. %
	\src{test\_plot\_hist}}

\figdouble{\textwidth}{daq-time-446}{m446: Down--sweep}
{\textwidth}{daq-time-447}{m447: Up--sweep}
{fig:daq-time}{Time--signals at selected field positions inside the hysteresis (Plusses)}{Measured Hall voltage %
	over time for $2048\,$s %
	after stopping at specific field %
	during down--/up--sweep, %
	respectively. %
	Solid lines show the time--signal %
	in front (bottom axis) %
	with a normalized histogram and it’s outlining KDE %
	in the background (top axis). %
	The time--signals are color--coded to fit the colors of Figure~\ref{fig:hist}. %
	\src{test\_time}}

\figtop{.99\textwidth}{daq-time-446-2}{fig:daq-time-446-2}{Time--signals  at selected field positions inside the hysteresis (Plusses, m446)}{Measured Hall voltage over time %
	after interrupting a down--sweep. %
	Solid lines show the time--signal %
	in front (bottom axis) %
	with a normalized histogram and it’s outlining KDE %
	in the background (top axis). %
	The time--signals are color--coded %
	to fit the colors of Figure~\ref{fig:daq-info-446}. %
	\src{test\_time2}}

In this experiment,
the measurement 
is paused mid--way through the field sweep 
while the sensor keeps measuring
to explore the MFN’s temporal development.
The stray--field’s temporal fluctuations 
at static fields may be
a more accurate indicator of the MFN, 
as they contain more extractable information
than the one used in the previous section.
This approach was inspired by Diao et al.
\cite{Diao.2010}.

The number of interruptions and measurements’ durations
defines how detailed the magnetic fluctuations inside the hysteresis loop are captured.
Therefore,
for the measurements in this section,
the angle of the external magnetic field $\theta$ has been set to $45^{\circ}$.
At this angle, the hysteresis loop is small enough
to measure the inside using 40 interruptions between $\pm 100\,$mT
without changing the lock--in’s sensitivity.

\subsubsection{Time--signals}
Figure~\ref{fig:hist} shows 
the kernel density estimation 
(KDE)\nomenclature{KDE}{kernel density estimation}
of the detected signals
at specific external field values noted aside.
The KDE serves the time--signal’s distribution $\Delta V_H$ around the mean
and is normalized to represent a probability distribution.
Shown here is only the noise--prone region 
right after passing the remanence.
As a reminder, the spikes 
only occur in this noise--prone region 
of the hysteresis loops
(as shown in Fig.~\ref{fig:hloop-45} to~\ref{fig:hloop-crosses}).

There are a few fields
where the signal has a single mean value and normal distribution.
This is the expected behavior, 
which can also be observed in other hysteresis loop regions 
and empty Hall signals.
Nevertheless,
it is more common in the selected region
that multiple mean values exist.
This is the first evidence for a non--ergodic signal.

Figure~\ref{fig:daq-time-446} shows the raw measured signal 
at respectively highlighted positions in Figure~\ref{fig:hist}.
These time--signals reveal several distinguishable steps
that indicate spin switching processes.
This result is pivotal to explain the spread $\delta V_H$ of the four experiments’ mean in Figure~\ref{fig:hloop-plusses}
as it represents temporal instabilities at static fields ---
metastable magnetization states inside the hysteresis loop.
Such spontaneous switching processes are known to appear
in thermally activated systems
\cite{Pohlit.2015}.
Evidence for such thermally activated switching processes
in similar CoFe nanostructures
has been found by Mohanad Al Mamoori
\cite{AlMamoori.2020}.
Consequential,
it is reasonable to suggest
that the spontaneous switching processes
are thermally activated.
In detail,
thermal activation processes
are statistically describe--able.
Therefore,
a bigger statistical ensemble of measurements
could uncover further interesting statistical properties.

\subsubsection{Analysis overview}

The chosen method of measuring the time--signal
yields large amounts of data.
An analysis overview over each measurement
gives the most informative details at a glance 
(see Fig.~\ref{fig:daq-info-446} and~\ref{fig:daq-info-447}).
The first plot (I.) 
displays the signal’s PSD 
in the unaltered axis design 
from the last section.\index{spectrumanalyzer!first spectrum}
The second plot (II.) 
shows the PSD as a contour plot with the different external field positions on the x--axis and the frequency on the y--axis.
As in the previous visualization, 
frequency and PSD axes are displayed logarithmically.

In the third plot (III.) of each analysis overview,
a linear regression applied at frequencies below $2\,$mHz
determines the PSD’s slope $\alpha$.
This slope is significantly elevated in Figure~\ref{fig:daq-info-446}
at the highlighted field positions of Figure~\ref{fig:daq-time-446}.
Two extra signals inside the noise--prone region 
disclose slopes larger than $1.5$.
are at external fields $-5\,$mT and $10\,$mT.
Closer inspection of the time--signal at those fields (see Fig.~\ref{fig:daq-time-446-2})
reveals a single confined step with a magnitude of multiple mV.
Other time--signals at larger fields do not exhibit such steps.

Comparable to the slope, the integral (IV.)
expresses the power of the noise.
This integral shows a relationship like $S_V (B) \sim B^2$.
This result reproduces expectations from previous findings
\cite{Korbitzer.2012,Li.Diss.2003}
and indicates the Hall device’s background noise.
The presented time--signals from Figure~\ref{fig:daq-time-446}
and~\ref{fig:daq-time-446-2} (down--sweep)
feature an elevated noise power
well above the background noise (see Fig.~\ref{fig:daq-info-446}).
Unfortunately,
this could not be reproduced with the noise powers or slopes
in Figure~\ref{fig:daq-info-447} (up--sweep).

\figtop{.95\textwidth}{daq-info-446}{fig:daq-info-446}{Analysis overview: Down--sweep (m446)}
{Multiple plots show extracted information from the analysis of MFN measurements during an interrupted field sweep. See text for details. \src{test\_info}}

\figtop{.95\textwidth}{daq-info-447}{fig:daq-info-447}{Analysis overview: Up--sweep (m447)}{Multiple plots show extracted information from the analysis of MFN measurements during an interrupted field sweep. See text for details. \src{test\_info}}

The fifth inset plot (V.) shows 
the time--resolved PSD at a single position inside the hysteresis.
This time--resolved PSD is automatically created 
through the \texttt{spectrumanalyzer}’s algorithms
(see Section~\ref{sec:data-spectrum}).
Similar to the PSD’s contour plot (II.),
the y--axis displays the frequency, and
the z--axis displays the PSD $S_V^{(n)}(f)$,
but the x--axis represents the temporal dimension.
The time--resolved PSD in Figure~\ref{fig:daq-info-446}
shows several identifiable spikes (dark blue)
at times $t < 400\,$s,
which,
compared with Figure~\ref{fig:daq-time-446} (right, blue time--signal),
determine temporal areas with considerable 
magnetization jumps.
Furthermore,
around $t \approx 750\,$s, 
a separate, smaller spike pinpoints
the last significant switching process
discoverable in Figure~\ref{fig:daq-time-446}.

The last inset plot (VI.) displays the captured signal
as an error bar over the external field position.
The error bar’s size is twice the variance of the signal ($\Delta V_H = 2 \sigma$).

\subsection{Method Comparison}

The results of both methods (SR785 and SR830DAQ) are compared
in equivalent situations
to ensure the comparability of both measurement techniques.
The experiments in this section follow the same approach as
related measurements with the signal--analyzer SR785 
(presented in Section~\ref{sec:res-sa}).

All measurements in this section investigate 
the Hall signal’s PSD
during the up--sweep
between $-25\,$mT and $+25\,$mT
after negative magnetic saturation.
The external field is applied at an angle of $\theta = 90^{\circ}$,
and the parallel measurement configuration is employed.

The experiments were performed with small variations:
The signal--analyzer (SR785) produces better results
with a pre--amplifier.
This pre--amplifier magnifies the detected signal by a factor of 20
but also augments its noise.
However, the SR830DAQ measures the noisy signal directly 
without an intermediate pre--amplifier.
The signal--analyzer also measured 
with a different time--constant setting in the lock--in
to acquire more extensive frequency ranges.
A time--constant of $\tau = 3\,$ms (SR785)
averages faster,
leaving more noise residues in the signal.
The SR830DAQ obtained the values at a time--constant of $\tau = 100\,$ms,
as a sampling rate of $8\,$Hz has proven to produce the best results.
These variations may lead to a scale difference of multiple magnitudes in the PSD.

\subsubsection{Sweeprates}
First,
both methods compare various sweeprates,
the velocity of the external magnetization’s change.
Figure~\ref{fig:sr785-sweeprates} shows the resulting PSD output of the signal--analyzer (SR785).
Except for the slowest sweeprate,
all other sweeprates unveil a noteworthy similarity.
Figure~\ref{fig:daq-sweeprate} shows the analysis of the SR830DAQ method.
Both methods exhibit a clear $S_V \sim 1/f^2$ behavior
for all sweeping measurements,
independent of the sweeprate.

A closer inspection of the insets,
specifically the calculated amplitude $S_{V_H} (f=1\,\mathrm{Hz})$,
exposes a correlation between the sweeprate and the PSD’s amplitude
beneath $d/dt (\mu_0 H_{ext}) \leq 1\,\mathrm{mT}/\mathrm{min}$.
This correlation is hardly detectable
and only becomes visible without the calculated fit,
when focusing on sweeprates $d/dt \mu_0 H_{ext} \leq 0.25\,\mathrm{mT}/\mathrm{min}$.
It is reasonable to conclude that 
a lower sweeprate is accompanied by
a clear decrease in the noise power,
at least for very low sweeprates.
However, considering the inset’s scale,
for sweeprates $d/dt (\mu_0 H_{ext}) \geq 0.5\,\mathrm{mT}/\mathrm{min}$
this statement should be considered uncertain.

Unfortunately, a software bug prevented posterior repetition of
such measurements at low sweeprates to determine the this behavior’s tenability.
In the attempt to repeat these low sweeprates’ measurements,
the EVE instrument module for the magnetic power supply \texttt{IPS120}
could not start the sweep of the magnet.
Consequentially, the black line displays a single measurement without any field change 
(at $\mu_0 H_{ext} = -25\,$mT with preceding negative saturation),
as opposed to the other’s $1/f^2$ behavior.


\figdouble{.89\textwidth}{sr785-sweeprates}{SR785}
			{.89\textwidth}{daq-sweeprate}{SR830DAQ}
			{fig:mfn-sweeprate}{Comparison of different sweeprates}{Repeated measurements of similar conditions %
				to compare both measurement techniques. See text for details. %
				\src{test\_compare\_sweeprates}}

\subsubsection{Current amplitudes}
\figdouble{.89\textwidth}{sr785-voltages}{SR785}
			{.89\textwidth}{daq-volt}{SR830DAQ}
			{fig:daq-volt}{Comparison of different current amplitudes}{Repeated measurements of similar conditions %
				to compare both measurement techniques. See text for details. %
				\src{test\_compare\_current}}

Figure~\ref{fig:sr785-voltages} shows the same $\pm 25\,$mT sweep--range for various driving current amplitudes.
It is expected that a current dependence
would scale the noise like $S_V \sim I^2$.
This expectation could not be verified with the signal--analyzer (SR785).
Instead, the PSD’s amplitudes are remarkably insensitive to changes in the applied current.
Although the inset’s result of the fitted PSD amplitude 
exposes a detectable correlation between the current and PSD,
this correlation is doubtful
when considering the PSD’s scale and omitted error bars.

Posterior repetition of the experiment with the SR830DAQ (see Fig~\ref{fig:daq-volt})
yields the expected behavior.
The fitted amplitude $S_V (f=1\,\mathrm{Hz})$
displays a clear $S_V \sim I^2$ relationship,
which also becomes evident in the PSD.

Interestingly, 
the slope $\alpha$ also displays a correlation with
the current $I$,
noticeable with both methods.
This correlation between $\alpha$ and $I$ is remarkable because
it has not been discovered in the literature yet
and is worth further investigation.

To summarize,
the measured hysteresis loops of the nano--tetrapods
are sensitive to the relative orientation of the tetrapods
towards the external field.
Subsequent repetition of identical experimental parameters
yields several nearly equivalent hysteresis loops
with identifiable noise--prone regions. 
In such noise--prone regions,
the magnetic stray--field 
at static states exhibits
spontaneous switching processes
observable via distinguishable steps 
in the time--signal.
The analysis framework \texttt{ana} provides an analysis overview
that grants insight into the most significant statistical details
of multiple measurements at once.
This analysis overview helps to quickly identify individual time--signals with spontaneous switching processes.
These switching processes are deduced to
indicate metastable magnetization states inside the hysteresis,
which may originate from thermally activation.
The frequency--dependent analysis of the MFN 
scrutinizes the signal’s PSD with a signal--analyzer
during a field sweep and 
displays a clear $S_V \sim 1/f^2$ behavior,
which only occurs when sweeping over noise--prone regions.
Noteworthy, this behavior is insensitive to changes of the temperature or external field’s sweeprate.
A comparison between both methods in similar conditions
displays a good reproduction of this $1/f^2$ behavior
with the new SR830DAQ method.