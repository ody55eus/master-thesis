\chapter{Discussion}\label{ch:discussoutlook}

The initial objective of this study was 
to identify and characterize
magnetic fluctuations.
As a test system, we chose
three-dimensional ferromagnetic CoFe nano--tetrapods.
These nano--tetrapods were grown via FEBID 
on top of a home-built micro-Hall sensor, 
a collaboration project with Prof. Michael Huth (Goethe University Frankfurt). 
In order to discover the nano--tetrapods’ specific properties,
the magnetic response 
to an altering external field
is scrutinized.
Two statistical analysis methods,
precisely a signal--analyzer SR785
and a novel data acquisition technique (SR830DAQ),
determine the signal’s PSD
to observe frequency--dependent noise.

The SR830DAQ technique utilizes previously
unused functionalities of the lock--in amplifier SR830
to dissect the lock--in’s time--signal directly.
The presented results
serve as a »proof of principle« demonstration
of this developed measurement technique.
It allows high--resolution measurements
of the Hall signal’s temporal development.
This temporal investigation reveals 
metastable magnetization states
in noise--prone regions,
which are directly observable in the 
acquired time--signal.
Additionally,
repetition of similar measurement conditions
corroborates findings of $1/f^2$ noise
when sweeping the field over noise--prone regions
near the remanence.
Follow--up experiments will validate the equivalence of both methods
on structures with well--known behavior.

The Hall signal’s PSD
can indicate noise characteristics
presumably emanating from these 
metastable magnetization states.
The introduced Python data analysis framework
\texttt{ana} provides an analysis overview for such 
interrupted MFN measurements,
which can help identify such
metastable magnetization states.
This analysis overview
revealed an elevated slope and power
in the noise of time--signals 
that contain observable steps.
Sizeable steps can be additionally
identified in the time--resolved PSD.
In other artificial spin--ice systems,
thermally induced magnetization switching processes
have been observed
\cite{Pohlit.2015}.
This investigation includes a statistical »survival time«
analysis.
Such an analysis could 
further scrutinize 
the metastable magnetization states’ statistical characteristics
and corroborate the deduction
that such metastable magnetization states
originate from thermal activation processes.

Furthermore,
the Hall signal averaged over a field sweep
inside a noise--prone region
always yields a 
characteristic $1/f^2$ spectrum,
possibly indicating domain wall motions
\cite[Ch. 9.3]{Bertotti.2008}.
These characteristic spectra are 
remarkably insensitive to
changes in the temperature or external field’s sweeprate.
Unlike Bertotti
\cite[Fig. 9.14]{Bertotti.2008},
the noise presented in this study
appears exclusively in very low frequencies
and does not exhibit a sweeprate dependence.

The correlation between the PSD and current amplitude
could only be observed
with the SR830DAQ method.
Measurements 
with the signal--analyzer (SR785)
of a pre--amplified signal
could not reveal such a correlation.
These contrasting results 
deserve further investigation.
Because of the metastable magnetization states
in the observed region,
the PSD of such measurements
is also more meaningful for larger statistical ensembles
than those presented.
Such larger ensembles would further allow the determination of
accurate error bars for the applied linear regressions.
However, \texttt{ana} does not yet analyze the error
of calculated fits.
The insets’ scales of presented PSD’s regression results
suggest that the error
explains single outlier.
The presented fitting results and their errors
highly depend on the range where the fit is applied.
Except where noted otherwise, the default fitting--range is between 
$20\,\mathrm{mHz} < f < 700\,\mathrm{mHz}$.
These values have proven useful to avoid the inclusion of artefacts.

The angle of the external magnetic field $\theta$
is determined through the Hall signal’s proportion 
to the maximum signal at $\theta = 0^{\circ}$.
Because of thermal instabilities and 
their influence on the Hall effect,
this proportion can not be determined
with exact confidence.
Therefore,
every change of the angle $\theta$ results
in an error of approximately $\Delta \theta \approx \pm 2-5^{\circ}$.
Subsequent measurements are always performed at identical angles,
but exact posterior reproduction of specific measurements 
at precise angles is only possible to a limited extend.

The magnetic stray--field’s calculation from the Hall voltage %
assumes an ideal squared active area of the Hall cross %
and a constant applied %
current of $I = 2.5\,\mathrm{\mu{}A}$.
Diffraction effects in the fabrication process result
in rounded edges (see Fig.~\ref{fig:nanostructures}).
The calculated magnetic stray--field 
should take a Hall response function $F_H (x,y)$ into account
to consider ballistic and diffusive transport regimes
\cite{Cornelissens.2002,Beenakker.1991}.
However, 
the data analysis in this thesis assumes the response function to be a constant 
$F_H (x,y) = 1$,
as the estimation demands extensive calculations
and exceeds this thesis’s scope.
This negligence results in a relative error,
which is presumed to be approximately $30\,\%$.
The effect of the applied current on the stray--field’s calculation is discussed next.

The applied current varies only slightly %
when applying the gradiometry technique. %
In this configuration, %
the limiting resistors $R_1$ and $R_2$ %
manipulate both applied currents $I_1$ and $I_2$, respectively.
These limiting resistors were usually adjusted between $1\,\mathrm{M\Omega} \geq R_{1/2} > 1.05\,\mathrm{M\Omega}$
in order to balance the gradiometry technique
for individual angles.
An applied voltage of $V_{in} = 2.5\,$V
results in an effectively applied current of $I_{1/2} \approx 2.5\,\mathrm{\mu{}A} - \delta I_{1/2}$
where the difference $\delta I_{1/2}$ to the assumed applied current
has an upper boundary of
\begin{eqnarray*}
\delta I_{1/2} \leq \frac{2.5\,\mathrm{V}}{1\,\mathrm{M\Omega}} -
\frac{2.5\,\mathrm{V}}{1.05\,\mathrm{M\Omega}} \approx 0.12\,\mathrm{\mu{}A}.
\end{eqnarray*}
The propagation of this neglected current difference $\delta I_{1/2}$
into the calculated stray--field is marginal.

The present study was designed to
comply with current best practices for reproducible and reusable research,
summarized in the introduction.
All acquired data, documentation, and algorithms
are available via the supplemental information (see Appendix~\ref{ch:supplement}).
The supplemental information was initially composed
to support the experimentalist’s future--self 
during the experiment.
After finishing the experiments,
a reasonable amount of time is invested
in adapting the supplemental information
to a presentable web--page
for reviewers and succeeding scientists.
External contributions receive credit, 
even without legal obligation (see Appendix~\ref{ch:legal}).
Except for EVE’s and \texttt{spectrumanalyzer}’s source--code,
every created code, figure, and text is licensed
(see Appendix~\ref{ch:legal})
to optimize re--use potential.
\texttt{ana} includes automated test cases using GitLab CI.

The measurements are solely analyzed with \texttt{ana},
making reproduction and re--use easier.
\texttt{ana} was initially created 
to analyze and visualize
the data acquired
in the course of this study.
The algorithms used to read and process the data
are customized for exercised filename
and file--structure conventions.
The source--code still contains several
equivalent passages that could be extracted
into separate functions.
Generalization of single customized passages
could establish a comprehensive analysis framework.

The introduction of a GitLab server improved
software development,
source--code control, and documentation management.
The GitLab server expedites code maintenance
and intensifies collaboration
if accepted by users.
User acceptance is a critical success criterion, 
as the benefits mentioned above only arise
when users welcome the new environment.
Therefore, user acceptance
determines GitLab’s success and impact on future research.

In recent years,
software products depend more than ever
on open--source components.
All open--source projects contain vulnerabilities 
that need years to be 
found, fixed, and updated
\cite{TheStateoftheOctoverse.16.12.2020}.
Proprietary software with closed--source concepts
also contains vulnerabilities 
that are not communicated and,
therefore, could radically exacerbate software security even further.
To fast--track the finding process of such vulnerabilities,
GitLab has launched a bug bounty program
\cite{HackerOne.25.01.2021},
motivating the community to delve into 
security tests searching for critical bugs.
Such a bug bounty program awards the finder of 
security issues with monetary appreciation,
facilitating faster security patches.

It is well known that all Docker containers 
inherit these above--mentioned security flaws
\cite{Walluhn.2020}.
This problem is universal and unavoidable,
regularly revealing severe exposures
\cite{Westernhagen.11.03.2020}.
State--of--the--art solutions to this issue 
include the setup of monthly updates,
monitoring systems
\cite{Bundesmann.16.12.2020},
and security scorecards
\cite{Roos.11.11.2020}.
The decision to provide supplemental information online
consequentially spotlights a risk--assessment of the employed server.
The risk of individual vulnerabilities is commonly assessed
by estimating its severity and occurrence probability
\cite{Kurtz.25.1.2021}.
For the employed server,
minimized risks are presumed.
In detail, high--entropy password protection of critical systems
and presumed scarcity of interest
diminish potential risks.
This and other possible improvements are highlighted next.


\section{Outlook}\label{ch:outlook}

As security flaws and crashed systems are always an issue,
preparation for data loss
is imperative.
The configured GitLab server includes a
built--in backup functionality.
This backup is configured
for automated weekly backups.
This automated backup is only stored on a local hard drive.
For future endeavours,
it is highly recommended to 
automatically store at least one recent backup offline and off--site.

The commitment for clean code and precise tests
can be advertised with a »CII Best Practice Badge«
\cite{GitHub.01.09.2020}.
Such a badge has critical requirements on code quality,
test coverage, and other project internal obligations.
All those at first glance seemingly negative liabilities
can amplify collaboration efficiency and
proliferate popularity --- effectively boosting the audience.

\figtop{\linewidth}{Gitlab-security}{fig:gitlab-security}{GitLab Vulnerability Report}{GitLab premium features allow to scan for vulnerabilities in Dockerfiles.}

To help fulfill the above--contoured obligations,
the open--source community provides excellent tools for 
improved automated testing
\cite{Python.tox}
and code analysis
\cite{Wiegenstein.21.10.2020}.
Recently, GitLab published free CI security templates with optional premium features.
These premium features are continually integrated
into the free version
\cite{Hahn.27.1.2021}.
The templates include license and vulnerability scanners\footnote{Result from license scanner for the \texttt{jupyterlab} Docker image is openly available: \url{https://gitlab.com/ganymede/jupyterlab/-/licenses}}.
Listing~\ref{lst:ci} shows how to apply the templates,
and Figure~\ref{fig:gitlab-security} shows 
the output of a premium vulnerability scan 
on the Jupyterlab Docker image.

Python provides various packages and APIs 
to improve visualizations
for enhanced data representation
\cite{EldarSultanow.16.12.2020}.
Some examples demonstrate how data can be differently presented nowadays
\cite{albi3ro.27.08.2020,Wolohan.2020,Healy.2019}.
Several open--source tools can connect a background Python kernel,
interacting with the user while displaying the visualization
\cite{Axtmann.2020}.
Utilizing such tools allows the presentation of an interactive visualization on a web--page.
Additionally, open--source tools can help scientists exploit 
further communication tools to expand narrative possibilities
and advance collaboration
\cite{Goodman.2017}.

If the GitLab server can be effectively deployed
and benefits a growing number of students,
collaboration could expand even further
through adaptation into a university--wide
GitLab server with premium functionality
for all students.
GitLab provides special educational licenses for such purposes
\cite{GitLab.Edu}
used by various universities
\cite{Bremen.Gitlab,Mainz.Gitlab,Halle.Gitlab,Hannover.Gitlab,Leipzig.Gitlab,Reading.Gitlab,Ulm.Gitlab,Wuerzburg.Gitlab}.

A large amount of static data is not supposed to be version controlled using git.
Unfortunately, in the restricted time--frame, no other tool seemed suitable.
For future endeavours, I would recommend a data version control system \cite{Besser.2020}.

In condensed--matter physics,
also the sample fabrication improves from advanced data science.
Novel machine learning approaches can optimize 
fabrication techniques of 
electromagnetic nanostructures and
nanoelectronic devices
\cite{Batra.2021,Greplova.2020,RuizEuler.2020,Kiarashinejad.2020}.

The presented data acquisition method enables new possibilities
to further scrutinize the time--signal.
In detail, 
there are numerous classical analysis tools that already provide programming interfaces for effortless usage
\cite{Cryer.2010,Schelter.2006}.
Additionally, 
novel time--series analysis methods utilize modern machine learning techniques and statistical models 
\cite{Lorenz.2020,Loening.sktime,Alexandrov.2019,Bontempi.2013,Cemgil.2011,Ahmed.2010}.
Machine learning algorithms could apply linear regression models $y(\vec{x},\vec{w}) = w_0 + w_1 \phi_1(x_1) + \cdots + w_N \phi_N(x_N)$
with various basis functions $\mathbf{\phi}$
to learn the ideal weights $\vec{w}$ for a given
time--signal
\cite{Bishop.2009}.
Such basis functions could consist of multiple trigonometric functions
to mimic a Fourier transformation.
Additionally,
extra basis functions, 
like the sigmoid function $\phi_n (x) = (1 + e^{-x})^{-1}$,
could help identify steps in the time--signal.