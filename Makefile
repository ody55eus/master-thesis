all: pdf

pdf:
	pdflatex -recorder thesis.tex
	bibtex thesis.aux
	makeindex  -o thesis.ind thesis.idx -s StyleInd.ist
	makeindex -s nomencl.ist -t thesis.nlg -o thesis.nls thesis.nlo
	pdflatex -recorder thesis.tex
	bibtex thesis.aux
	pdflatex -recorder thesis.tex
	pdflatex -recorder thesis.tex
clean:
	rm -f *.log *.aux *.dvi *.lof *.lot *.bit *.idx *.glo *.bbl *.bcf *.ilg *.toc *.ind *.out *.blg *.fdb_latexmk *.fls *.run.xml
