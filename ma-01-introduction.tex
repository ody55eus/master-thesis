\chapter{Introduction}\label{ch:intro}
\vspace{-1mm}
%\begin{dBox}
	\begin{quotation}
		\begin{quote}
			»As students of Physics we observe phenomena under varied circumstances,
			and endeavour to deduce the laws of their relations.
			Every natural phenomenon is, to our minds, the result of an infinitely complex system of conditions.
			What we set ourselves to do is to unravel these conditions, and by viewing the phenomenon in a way which is in itself partial and imperfect,
			to piece out its features one by one, beginning with that which strikes us first,
			and thus gradually learning how to look at the whole phenomenon so as to obtain a continually greater degree of clearness and distinctness.
			In this process, the feature which presents itself most forcibly to the untrained inquirer may not be that which is considered most fundamental by the experienced man of science;
			for the success of any physical investigation depends on the judicious selection of what is to be observed as of primary importance,
			combined with a voluntary abstraction of the mind from those features which, however attractive they appear, we are not yet sufficiently advanced in science to investigate with profit.« \cite{Maxwell.1870}
		\end{quote}
		\textbf{James Clerk Maxwell} (1870)
	\end{quotation}
%\end{dBox} %

Throughout the past century, there has been a rapid development in science and industry.
Many communities struggle to keep up with these rapid technical developments since the emergence of computers.
The computer has grown a powerful working tool and influenced human society on many levels.
As predicted by Feynman \cite{Feynman.1960} and Moore \cite{Moore.2006}, computing power has improved exponentially for over half a century.
Irrespective of the approaching end of Moore's law, 
information technology embraces the possibilities to grow in various other disciplines
\cite{Williams.2017,Leiserson.2020}.


Condensed--matter %
\marginline{Electronics and material science}%
physics and material science have contributed during the last decades to this advanced growth with quantum materials 
\cite{Basov.2017,Tokura.2017}
that allow novel spintronics devices
\cite{Xu.2016,Sato.2015,Pulizzi.2012,Chappert.2007}, 
utilizing the electron’s spin for semiconductor components with tailored properties.
This allows the combination of logic operations and data storage for novel in--memory 
\cite{MigliatoMarega.2020,Sebastian.2020,LeGallo.2018}
and brain--inspired (neuromorphic) 
\cite{Zhang.2020b,Roy.2019}
hardware designs.
Many recent advancements in information technology are based on such discoveries.
The resolution of detectors has
significantly improved primarily by 
component miniaturization
down to the micro--and nanoscale
\cite{Flohr.11.04.2020}.
At these scales,
quantum phenomena emerge, such as the wave--particle duality or the uncertainty principle.
Investigating these and related phenomena
led to considerable progress in
nanomagnetic material research
\cite{Skomski.2003},
notably 
two--dimensional materials\index{2D Materials} 
\cite{Liu.2020} 
and three--dimensional nano--magnetism\index{3D Nanostructures}
\cite{FernandezPacheco.2017}.
These nanomagnetic materials 
contribute to the
detailed understanding of the magnetization dynamics
\cite{Barman.13.08.2020}
of fundamental magnetic structures, 
like domain walls, vortices, skyrmions, and magnetic monopoles
\cite{Tokura.2020,Braun.2014,Fert.2013,Jaubert.2011}.
Material specific properties are 
often too complex to find analytical
or even numerical solutions for problems, 
like identifying the phase diagram
\cite{Bausch.2021}.
Instead, such properties can be detected
through a comprehensive investigation
of novel materials and structures
in various environments.
The investigation of such novel materials and structures requires the development of high--resolution experiments
and simulations.
Both endeavours rely highly on efficient computers and algorithms.

I \marginline{This Study}
developed a novel method to measure
magnetic fluctuations 
and analyze them using a self--written
object--oriented Python programming framework \textit{\texttt{ana}}.
A programming framework is 
an implemented collection of algorithms 
to provide a stable code for consistency 
and allows users to focus on areas of expertise.
Object--oriented frameworks can build on 
class hierarchies, inheritance, and encapsulation 
to increase extension and re--use opportunities
\cite{Simmonds.2012,Yang.1997}.
{\texttt{ana}} is such a framework
inspired by CERN’s ROOT data analysis framework
\cite{ROOT.Guide}.
\texttt{ana} is necessary to handle the data of over 500 recorded magnetic measurements
and allows easy access to various measurement details, analyses, and visualizations.
\texttt{ana} is designed as free\nomenclature{free software}{»The users have the freedom to run, copy, distribute, study, change and improve the software.«
\cite{FreeSoftwareFoundationInc.et.al..2018}} 
and open software
for best re--use and reproduction of results.
This goal can be accomplished with
state--of--the--art data science tools and methodologies.


\section{Computational Research}\label{sec:bestpractice}

Data\marginline{Data--driven research} 
science is a relatively new and interdisciplinary scientific discipline \cite{Berkley.DataScience,DataScience.culture} and is currently promoted by publishers \cite{Nature.2020,Hawkins.2020,Nature.ScientificData}.
One goal is to improve scientific research using computational skills with a focus on reproducibility and reusability.
Many computational disciplines facilitate 
open\nomenclature{open}{»Open means anyone can freely access, use, modify, and share for any purpose (subject, at most, to requirements that preserve provenance and openness).«
\cite{Open.Definition}} 
science and open data\footnote{See also »open« definition in Nomenclature.}
\cite{cos.hp,Chen.2019,Pritychenko.2020}
to increase productivity and collaboration
\cite{Lowndes.2017}.
Data--driven research
focuses on every stage of the 
data life cycle
\cite{Stodden.2020,Berkley.DataScience}, 
including acquiring, maintaining, processing, analyzing, and communicating the data.
Because of the importance of 
high--quality software and data 
in modern research and,
in the self--reliance of support, 
computational best practices are an evolving field and deserve a short review
\cite{ICERM.Report,Goodman.2014,Stodden.2014,Wilson.2016,Science.Code}.

\begin{eBox}[frametitle={\textbf{Box 1} | Best Practices for Reproducible and Reusable Research}]
	\begin{description}
		\item[\textbf{Principles}] Include data, algorithms, or other central or integral information in the publication.
		If this is not possible, it should be made freely accessible through other means \cite{Stodden.2014}.
		\item[\textbf{Workflow}] Tracking and documenting the workflow is vital to enable reproducibility and re--use by others \cite{Stodden.2014}.
		\item[\textbf{Data}] All data should be \textit{findable, accessible, interoperable, and reusable} (\textit{FAIR}) \cite{Wilkinson.2016,GOFAIR.18.08.2020,ResearchDataAllianceFAIRDataMaturityModelWorkingGroup.2020}.
		The original data should be kept intact, version--controlled, enriched with metadata, and shared with a permanent identifier \cite{Goodman.2014,EU.EOSC.2017,EU.2018,EU.OpenScience}.
		\item[\textbf{Code and Methods}] Source code must be available and accessible using version control
		\cite{Science.Code,Perkel.2019,Baker.2016,Morin.2012,Barnes.2010}.
		All results should include input values and other parameters with code and scripts \cite{Stodden.2014}.
		\item[\textbf{Guidelines}] All researchers should follow their associated data and code sharing guidelines \cite{DeutscheForschungsgemeinschaft.2019,NationalScienceFoundation.2015, NationalScienceFoundation.2020,IEEE.Software,nsf.sharing}.
		\item[\textbf{Licensing}] Data and code should always be made available for re--use through open licensing \cite{Stodden.2009,OpenSource.Licenses,GitHub.Appendix,GitHub.Licenses,Stodden.2014}.
		\item[\textbf{Test and Automate}] Suitable test functions can automatically test the code using continuous integration services \cite{Perkel.2020,Perkel.2018}.
		\item[\textbf{Credit}] All contributions to a project should be acknowledged, and data and software should be cited
		\cite{Casari.2021,Holcombe.2019}.
		Code can and should be re--used and adapted under an Open Source Initiative (OSI) approved license \cite{OpenSource.Definition,Stodden.2009,GitHub.Appendix,GitHub.Licenses,Stodden.2014}.
	\end{description}
\end{eBox}

Data 
\marginline{Data in science} 
is the source of every scientific discovery, and its quality determines the power and success of an investigation.
As stated by Maxwell above, researchers focus on finding striking evidence in gathered data.
In this process, a mindful selection of data is necessary to convince the scientific community successfully.
However, an equal spotlight should be directed to the 
\textit{findability, accessibility, interoperability, and reusability (FAIR)}\index{Data!FAIR Data}\nomenclature{FAIR}{findability, accessibility, interoperability, and reusability}
guiding principles for scientific data management \cite{Wilkinson.2016,GOFAIR.18.08.2020,ResearchDataAllianceFAIRDataMaturityModelWorkingGroup.2020,Pasquier.2017}.
These principles realize good research practices 
\cite{DeutscheForschungsgemeinschaft.2019,NationalScienceFoundation.2020,NationalScienceFoundation.2015,nsf.sharing}.
Many universities
\cite{Hettne.2020,Harvard.Data,Reading.Reproducibility} and networks 
\cite{Choi.2020,Chen.2019,Gleeson.2017,Lowndes.2017,Muller.2015,DigitalScience.2017,EU.OpenScience,EU.EOSC.2017,EU.2018}
have already adapted the FAIR principles into their scientific routine, 
and assessing methodologies have been developed 
\cite{Bahim.2020,Assante.2016,Garcia.2016}.
Data transparency holds excellent prospects in science 
\cite{Aczel.2020,Hanson.2011,Science.2011} 
and positively impacts publications’ success
\cite{Vandewalle.2012}.
A recent publication \cite{Greaves.2020}
shows a statistically significant amount of phosphine gas on venus (implicating possible extraterrestrial life),
which could not be reproduced
\cite{Snellen.19.10.2020,Witze.2021}.
This additionally highlights the importance of data availability and reproducibility in modern research.
A selection of guides 
\cite{Goodman.2014,Librarycarpentry.TopFAIR, DataScience.culture,terms4fairskills.github.io.20.09.2020}
and examples 
\cite{Gregory.2020,Elsevier.2020,ICERM.Report,DigitalScience.2017,Jennings.2004}
emphasize the importance of data availability.
The fact that even the slow grinding mills of politics
nowadays publicizes an intensified interest in FAIR principles
\cite{GAIA.X,EU.2020,EDISON.2017,EU.EOSC.2017,EU.2018,EU.OpenScience,EU.openresearch,Datenethikkommission.2020,Krempl.27.1.2021,JessWhittlestone.2019,Feilner.18.11.2020}
ultimately highlights their prominence.

A \marginline{Reprodu\-cible research}
debate has gained interest throughout the last years
concerning a 
\textit{reproducibility crisis}
\cite{Nature.reproducible,Nature.2012,McNutt.2014,Buck.2015,Baker.2016b,JonBrock.28October2019}.
While mainly concerned with medical and social sciences \cite{Dirnagl.2020,Begley.2012} or statistical mis\-inter\-pre\-tations \cite{GemmaConroy.2019,Leek.2017,Leek.2017b,Nuijten.2016,Leek.2015,Nuzzo.2014}, all computational sciences
\cite{Merali.2010}
are affected.
The problem’s primary source is missing documentation and unmaintained code,
which leads to difficulties when hard--or software gets out of date
\cite{Perkel.2020, Hinsen.2020,Hinsen.2020b}.
Computational sciences made various contributions to tackle this problem introducing workflows
\cite{Perkel.2019c,Lowndes.2017}
and guides 
\cite{Stodden.2020,Bjornson.2019,Wilson.2016,Stodden.2014,Sandve.2013,Noble.2009,Reading.Reproducibility}
for best practices.
Interested readers are referred to additional literature 
\cite{Stodden.2020b,Raff.2020,Shen.2014,Barba.2012,Stodden.2012,Donoho.2009,Wilson.2006,Schwab.2000}
and Box 1.

\subsection{Open Source Tools}

Python \marginline{Data science with Python}%
is an object--oriented interpreter based 
programming language
and an ideal tool for scientists 
\cite{MarcusHanhart.16.12.2020,Perez.2011,McKinney.2017,Vanderplas.2016}.
It combines high--level flexibility and readability 
with low--level capabilities, 
like linking to third--party libraries in C, C++, or Fortran %
\cite{Behnel.2011,McKinney.2010}.
The open--source community has contributed many Python modules to ease access to these low--level high--efficiency libraries
\cite{Kreiling.2020,Harris.2020,Virtanen.2020,vanderWalt.2011,FabianPedregosa.2011,Perez.2007,Hunter.2007}.
The Python documentation generator Sphinx
\cite{Brandl.2007}
can combine docstrings from Python packages
with other reStructuredText or markdown files
and convert them into a user--readable format.
This tool documents the data and code \textit{workflow}\index{Data!Workflow} for distribution as a web page or printed document.
In large software projects, it is common for the lines of documentation to exceed the code lines.
A complete list of the used tools is available in Appendix~\ref{ch:legal}.

GitLab \marginline{Continuous Analysis}
\cite{GitLab}
provides a single open--source tool to version control files with vast adaptability for collaboration.
It is similar to the popular GitHub
\cite{GitHub}
and builds on the popular decentralized version control software git
\cite{git-scm,Loeliger.2009,Potter.2016}.
Comparable to GitHub Actions, GitLab is equipped with \textit{Continuous Integration / Continuous Development (CI/CD})
\cite{Brown.2016}\nomenclature{CI/CD}{Continuous Integration / Continuous Development} tools
for the entire »DevOps« life cycle,
which can be perfectly integrated with Python
\cite{Lenz.2019}.
Container technologies like Docker
\cite{Boettiger.2015,Perkel.2019}
allow creating virtual environments with defined software versions
that perform in a repeatable universal way.
Uniting Docker containers with CI/CD 
allows the automatic re--execution in a well--defined environment
\cite{Friedrich.20.3.2019,Hecht.26.11.2018}.
»\textit{Continuous Analysis}«\index{GitLab!Continuous Analysis}
describes a workflow that applies this combination to rerun a computational summary automatically after data or code has been updated
\cite{BeaulieuJones.2017,Perkel.2019c}.

Lastly, \marginline{Legal framework}% 
trying to publish the data and source code naturally raises questions regarding licensing\index{Licensing}
\cite{Stodden.2009}.
From the legal perspective, 
every creative work is protected by the German »Urheberrecht,« 
which includes special treatment to source code
\cite{Urheberrecht.gesetz}.
Raw measurement data do not apply to this law;
however, an original selection and arrangement of the data can be licensed as a collection.
Such collections and
more creative works other than programming code
can be comfortably shared publicly.
For such a purpose,
the nonprofit organization »Creative Commons« 
provides various licenses and tools 
that quickly grant copyright permissions
for such creative works
\cite{CreativeCommons.Share}.
On the other hand, software and source code
can be shared and protected
amidst multiple terms of conditions and limitations
\cite{GitHub.Licenses,GitHub.Appendix}.
The literature argues that a copyleft (share--alike) concept shall be inappropriate for the scientific context
\cite{Stodden.2009}.
Nevertheless, I decided for personal software projects to 
publish small scripts using the MIT license
\cite{MIT.Overview}
and preserve larger projects by exercising the 
obligations of the GNU\nomenclature{GNU}{recursive acronym for »GNU's Not Unix«}
General Public License (GPL)
\cite{Barbulescu.2011,FreeSoftwareFoundationInc.et.al..2018}.\nomenclature{GPL}{General Public License}
The GNU GPL protects the rights of software users by
	\begin{itemize}
		\item[a)] giving them the irrevocable rights of usage, modification, and redistribution of the software; and 
		\item[b)] requiring developers on redistribution to provide the full source code and documentation of changes and functionality;
	\end{itemize}
while legally supporting software developers through patent and warranty clauses.
The included obligations preserve openness and assist collaboration.
Appendix~\ref{ch:legal} highlights some permissive and share--alike licenses.
Such legal actions are necessary to grant 
interested readers the rights to scrutinize
an investigations’ findings.
The findings of the following study are
available via the supplemental information (see Appendix~\ref{ch:supplement})
and concern the magnetic responses’ scrutinization
of three--dimensional nano--tetrapods.

\section{Nanomagnetism}

Geometric
\marginline{Low--dimensional nano\-structures}%
restraints can group nanomagnetic systems into different dimensions.
Single domain nanoparticles (zero--dimensional)
\cite{Bedanta.2013}
are promising candidates for biomedicine
\cite{Tartaj.2005}
and spin--transfer devices
\cite{Silva.2008,Piraux.2007}.
Magnetic nanowires (one--dimensional) can be applied for racetrack--memory
\cite{Parkin.2015,Parkin.2008}
and logic devices
\cite{Allwood.2005}.
Gaining interest emerged for the topologically interesting 
thin films (two--dimensio\-nal)\index{2D Materials}
with observations of 
complex magnetization states
\cite{Liu.2020,Romming.2013,Ma.2011}.
These new phenomena are investigated using
multi thin film layers, heterostructures, and magnetic tunnel junctions
\cite{Gibertini.2019,Wang.2013}.

Particular two--dimensional
\marginline{Spin--Ice systems}%
lattices, 
like a squared or kagome lattice,
triggered magnetic frustration effects
\cite{Lhotel.2020},
the geometrically enforced prevention of order,
which results in residual entropy at vanishing temperature.
Similar behavior is observed in water ice
\cite{Pauling.1935},
which led to the new class's name:
\textit{spin--ice}\index{Artificial Spin Ice}
systems
\cite{Bramwell.2020}.
Customized fabrication
further expanded the number of scrutinize--able systems
\cite{Lendinez.2020,Skjrv.2020}.
Advancements towards 
%\marginline{3D Nanomagnetism}
\textit{three--dimensional nanostructures}\index{3D Nanostructures}
\cite{Fischer.2020}
open the opportunity to investigate several interesting geometries,
including the pyrochlore lattice, a diamond--like lattice.
For example, such lattices allow the examination of
magnetic monopoles
\cite{Kruglyak.2010,Castelnovo.2008,Bramwell.2001}.

In \marginline{Fabrication techniques} the center of this research are such artificial three--dimensional nano--magnets.
There are numerous methods available to grow such structures
\cite{Fischer.2020,Teresa.2014}.
This study concentrates on magnetic nano--tetrapods\index{Tetrapods}
grown by means of
\textit{focused electron beam induced deposition} (\textit{FEBID})\index{focused electron beam induced deposition (\textit{FEBID})}
\cite{Plank.2019,Huth.2012,Randolph.2006,vanDorp.2008}
in the group of Prof. Michael Huth (Goethe University Frankfurt)
\cite{Huth.2020,FernandezPacheco.2020,Fowlkes.2018,Porrati.2015}.
This technique facilitates various geometries, materials, and tailored characteristics
\cite{Gibertini.2019,PabloNavarro.2017,Weirich.2013,Lavrijsen.2011,FernandezPacheco.2009},
making artificial spin ice structures
\cite{Keller.2018,Pohlit.2016b}
and other functional electronic components
like Hall sensors
\cite{Candini.2006,Gabureac.2010}
possible.
Diverse techniques involve photon-- or ion--based deposition methods
\cite{Hunt.2020,Williams.2017b,Gabureac.2010,Candini.2006}.
The first--generation of the investigated nano--tetrapods created by FEBID  is well documented in the literature
\cite{Huth.2019,Keller.2018b}.
Early studies of this first--generation found a strong magnetic coupling between different magnetic nano--tetrapods
\cite{Keller.2018,AlMamoori.2018,AlMamoori.2020b,AlMamoori.2020}.
Such strongly correlated systems exhibit phase transitions,
which can be explored through numerous methods
\cite{Paschen.2021}.

Resolving 
\marginline{Magnetic measurements}
small magnetic moments with a high resolution is a difficult task.
Most techniques involve the sensitive detection of magnetic stray--fields, 
reflecting the total magnetization of nearby structures.
Superconducting quantum interference devices (SQUIDs)\nomenclature{SQUID}{Superconducting Quantum Interference Device}
\cite{Clarke.1994}
and Hall devices
\cite{Popovic.2004,Hengstmann.2001,Bending.1997}
are susceptible tools for such measurements.
SQUIDs are more sensitive than Hall sensors
\cite{Mitchell.2020}
but can not be employed in similar flexible temperature ranges.
Hall sensors can be fabricated 
of different materials for several purposes
\cite{Dauber.2015,Gabureac.2010,Candini.2006}.
This study concentrates on stray--field studies using a 
GaAs/AlGaAs\index{AlGaAs}
\textit{micro--Hall magnetometer}
\cite{Muller.2006b,Li.Diss.2003,Li.2004} (see Chapter~\ref{ch:exp}).
Such devices form a 
two--dimensional electron gas (2DEG)\index{two-dimensional electron gas (2DEG)}
with high mobilities of electrons at low temperatures
\cite[Ch.~10.2.]{Gross.2012}.
This makes them ideal candidates for the sensitive detection of magnetic stray--fields at low temperatures
\cite{Chenaud.2016,Li.2002}.

Despite
\marginline{Noise}
being often seen as an unwanted relict,
noise can contain valuable information about a system
\cite{McDonnell.2011,Sethna.2001,Muller.1990}.
It is usually investigated employing \textit{fluctuation spectroscopy},
transforming the signal
into the frequency domain using
a Fourier transformation
\cite{Welch.1967}.
Electronic noise comprises various sources,
like magnetic domains and spin fluctuations,
charge carriers crossing an energy barrier,
or other material effects
\cite{Muller.2011}.
All electronic resistors at finite temperatures 
exhibit thermal noise, 
which was first observed and described by Johnson
\cite{Johnson.1928}
and Nyquist
\cite{Nyquist.1928};
and can be utilized as a thermometer
\cite{Patronis.1959,Rothfu.2013}. %
\marginline{Random telegraph signal:\\
	\tikz[scale=.3,overlay,baseline=.8ex]{
		\draw[<->] (-8.2,1.2) node{}
		-- (-8.2,-.2) 
		-- (-6.2,-.2) node[pos=1.7,below=-3pt]{\tiny Time};
		\draw (-8,0) 
		-- ++(1,0)
		-- ++(0,1)
		-- ++(1,0)
		-- ++(1,0)
		-- ++(0,-1)
		-- ++(1,0)
		-- ++(0,1)
		-- ++(1,0)
		-- ++(1,0)
		-- ++(0,-1)
		-- ++(1,0);
}}%
Together with the current dependent shot noise (after W. Schottky) 
\cite{Schottky.1918}
and quantum shot noise
\cite{Beenakker.2003},
this represents the class of »white« noise,
which is frequency--independent up to a specific cutoff frequency.
On the other side,
a random telegraph signal (see margin)
leads to a Lorentzian spectrum ($S \sim 1/f^2$)
with a characteristic corner frequency 
(\textit{Generation--Recombination--Noise}).
Theoretically, this is described by
two--level fluctuation processes 
with different time--constants
\cite{Machlup.1954}.
In nature, a universal \textit{$1/f$ noise} behavior is observed,
often described as a
superposition of these processes,
dominating at low frequencies
\cite{Wolf.1997,Musha.1976,VOSS.1975}.
All noise contributions mentioned above
cumulate in laboratory conditions 
and deserve a closer inspection.

Studies 
\marginline{Sensor noise}
over the past decades have provided crucial information about noise in semiconducting devices
\cite{Grasser.2020,Balandin.2013,Itoh.2001}.
Previous research has established that 
noise in semiconducting Hall sensors and SQUIDs
scales with the external field
\cite{Vaes.1977,Kleinpenning.1980,Jabdaraghi.2017}.
In Hall sensors,
the noise can additionally be used to determine
the sensitivity
\cite{Schaefer.2020}.
In micron--and sub--micron--sized GaAs/AlGaAs Hall devices,
the aforementioned $1/f$ noise
dominates at low frequencies
\cite{Muller.2015b,Hicks.2007,Muller.2006,Li.2004,Li.Diss.2003}.

Magnetic \marginline{Magnetic flux noise}
noise reveals more information about
magnetization dynamics
\cite{Budrikis.2012,Ozbay.2009,Shpyrko.2007,OBrien.1994}.
In particular, theoretical calculations indicate that magnetic monopole noise is detectable in artificial spin ice systems
\cite{Dusad.2019,Klyuev.2017}.
There are relatively few historical studies in the area of 
\textit{magnetic flux noise} (\textit{MFN}).
Most previous studies focused on detecting
MFN in SQUIDs
\cite{Jabdaraghi.2017,Anton.2013,Ferrari.1988}
or other superconductors
\cite{Saitoh.2004,Tsuboi.1998,Ferrari.1994}.
Further investigations on magnetic Barkhausen noise
revealed a characteristic $1/f^2$ behavior
that this study aims to reproduce and scrutinize
\cite{Bertotti.1994,Alessandro.1990,Alessandro.1990b,Alessandro.1988,Bertotti.1981}.

\begin{figure}[t!]
	\begin{subfigure}[b]{0.46\textwidth} \centering
		\includegraphics[width=\linewidth]{mindmap_data}
	\end{subfigure}
	\begin{subfigure}[b]{0.53\textwidth} \centering
		\includegraphics[width=\linewidth]{mindmap_phys}
	\end{subfigure}
	\caption[Mindmap structure of this thesis]{\textbf{Mindmap structure of this thesis}. %
		The following thesis utilizes the data science toolbox (left) to investigate the noisy magnetic characteristics of three--dimensional nano--tetrapods (right). %
		Balls are highlighting programmed software tools (left) and experimental methods (right). %
		Black annotation boxes below major topics refer to the corresponding sections in this thesis.
		The study’s primary focus is emphasized in black font.\label{fig:mindmap}}
\end{figure}


This\marginline{Thesis content} study offers insight into 
such fluctuation measurements of the measured Hall signal.
Further scrutinization of the time--signal 
at static states inside the hysteresis
reveals spontaneous switching processes,
indicating \textit{meta\-stable magnetization states} (see Chapter~\ref{ch:results}).
This is the main incentive of combining the 
two established preceding measurement techniques,
micro--Hall magnetometry and 
fluctuation spectroscopy,
to scrutinize the magnetic flux noise of 
three--dimensional nano--tetrapods.
The
following thesis focuses on the challenges 
to perform each step of the research process 
\textit{lege artis} \cite[Guideline 7]{DeutscheForschungsgemeinschaft.2019}, explaining the continuous quality assurance mechanisms taken in maintaining, handling, and documenting scientific data.
Figure~\ref{fig:mindmap} highlights the primary topics of each part.
Chapter~\ref{ch:data} outlines the design of 
EVE and \texttt{ana} as main software tools,
following by the continuous analysis workflow
to integrate these tools into the daily routine (left).
The experimental measurement details (right) are
explained in Chapter~\ref{ch:exp}.
Chapter~\ref{ch:results} presents the analyzed results, which are subsequently discussed in Chapter~\ref{ch:discussoutlook}.
Closing suggestions for future endeavours are provided in Chapter~\ref{ch:outlook}.
Chapter~\ref{ch:concl} summarizes key points and conclusions. 
