\chapter{Continuous Analysis}\label{ch:data}
\vspace{-18mm}
\begin{cBox}[frametitle={Brief Summary}]
	\begin{itemize}
		\item I updated and improved the essential 
			software to measure, control, and acquire data 
			(\textbf{EVE}).
		\item A novel Python data analysis framework (\textbf{\texttt{ana}})
			calculates and visualizes measurements.
		\item \textbf{GitLab} introduces a server--based distributed version control and documentation platform.
		\item \textbf{Docker} allows virtualization and well--defined software versions.
		\item \textbf{Continuous Analysis} combines Docker with {GitLab Continuous Integration} (CI) for automated analysis
		and better reproducibility.
	\end{itemize}
\end{cBox}

Data are vital facts for every scientific discovery
and need special attention.
Data are becoming more decisive nowadays 
with cheap storage
and novel cloud computing capabilities
that rely on big data,
collecting and combining data
on a colossal scale.
The following chapter concentrates on the challenges
of the data life cycle.
First, the data acquisition 
introduces EVE (short for Efficient Virtual Environment),
the primary measurement program used to control the instruments.
I enhanced, updated, and documented EVE intensively during this thesis.
The enhancements enable unused functions in existing instruments
and increase stability and debug--ability.
The data, programming code, and corresponding documentation
are stored on a self--maintained GitLab server.
Data analysis is based on the self--programmed Python data analysis framework (\texttt{ana})
and automated for repeatability
utilizing Continuous Integration (CI\nomenclature{CI}{Continuous Integration}) and Docker.


\section{Data Acquisition}\label{sec:data-daq}

Data acquisition (DAQ)\nomenclature{DAQ}{data acquisition}
is the sampling of a signal,
typically digitalizing it for post--processing.
Here, this task is performed by the group--internal measurement program EVE.
It communicates with the instruments to command and read the present state.
It reads each selected instrument's currently observed value
in pre--defined time intervals
and saves it into a comma--separated--value (csv) file.\nomenclature{csv}{comma-separated-value}


\subsection{Established Tools}

EVE\index{EVE}
\marginline{EVE}
was programmed in collaboration by this working group’s students over the last years and continuously extended
\cite{AdhamAmayan.2013,SarahOttersbach.2013,BenediktHartmann.2017,JonathanPieper.2017,Kammerbauer.2020}.
The graphical user interface (GUI)\nomenclature{GUI}{graphical user interface}
of EVE
is based on the popular PyQt project
and supports adding multiple instruments to a measurement process.
Each instrument provides its own GUI, 
which is created when it is added to the instrument list.
An instrument can be added multiple times to command various devices of the same type.

\figy{.9\textheight}{eve-structure}{fig:eve-structure}{Functional flowchart diagram of EVE’s fundamental objects}{The main program (top) consists of multiple different threads. Each instrument class (bottom) is initialized as a single thread.}

Figure~\ref{fig:eve-structure}
\marginline{Flowchart}
displays a schematic view of EVE’s internal design.
The yellow arrows indicate function calls by another part of the program, while the green arrows indicate data transfers, e.g., writing data into a variable or file.
The upper half shows the main program, 
which consists of multiple classes and threads.
The main program connects all buttons and visual inputs with the corresponding classes and
creates an instance for each initialized class.
Some classes inherit threading capabilities from PyQt5’s \texttt{QThread}.
Threading allows Python to break out of the sequential execution
and jump between different threads.
With today’s multi--processing capabilities,
this threading approach can be improved with
Python’s similar core--library \texttt{multiprocessing}.
These threading and multi--processing capabilities
allow the integration of various instruments.

While \marginline{Instruments}
EVE can handle multiple different instruments, 
most user modifications are limited to an individual instrument.
The lower half of the flowchart shows a single instrument class with the required functions and procedures.
This system of separate modulated instruments allows users to easily modify and add new instruments without understanding all programming backgrounds of EVE.
Each instrument class serves a tailored GUI 
that is embedded into the instruments tab of EVE.
If available, most instruments also provide an Auto GUI (right)
that allows automated measurement routines\index{EVE!routine}.
A routine consists of two matrices (right--top) that represent
a list of scheduled commands.
Such a routine can be easily programmed with EVE
and saved as a text file for re--use.
Unfortunately,
EVE 3.0 alpha was unable to read previously saved
routines.
This problem could be solved over time,
but not all used routines have been saved with the measurements.


\subsection{Enhancements}
EVE \marginline{Updates} was initially programmed in Python 2 using PyQt4 and other packages\nomenclature{Python package}{»A Python module which can contain submodules or recursively, subpackages.«
\cite{Python.glossary}} from the outdated Python(x,y) project\footnote{Python(x,y) is a deprecated collection of scientific tools for data analysis and data visualization. See \url{https://python-xy.github.io/} for further information.}.
This project has been discontinued and is not updated anymore.
Therefore, to update EVE with new features and include recent packages, the code needed to be upgraded.
This is especially challenging
when facing renamed classes and functions
in the Python packages PyQt5, pyvisa, and matplotlib.
PyQt5 additionally changed some inheritances and moved
functionalities between main modules\nomenclature{Python module}{»An object that serves as an organizational unit of Python code.«
\cite{Python.glossary}}.
To fully document the code changes made,
I installed a local GitLab server 
providing version control for EVE since version 2.6.3.
GitLab\index{GitLab} 
\cite{GitLab}
implements git\index{GitLab!git}
\cite{git-scm}
and makes details findable
through a user--friendly web interface.
I also ensure an easy installation of EVE
through Anaconda.
Together with Anaconda, GitLab provides
easy access to all EVE versions
with the needed software requirements.
Anaconda maintains various separated environments
with a package manager.
The package manager determines all dependencies
when installing new software.
This ensures the stability 
of the interplay between different packages.
Anaconda’s environments allow a more straightforward setup for EVE.

Further \marginline{Functional updates}
EVE enhancements developed throughout this study
include functional updates.
The Python core--library \mbox{\texttt{logging}}
improves the debugging process by
saving relevant debug messages into a log file.
This improves the debugging process and increases developing time.
Optionally added 
command line arguments\footnote{For example: 
	\texttt{python EVE.py {\color{blue!70!black}-d}} activates the debug mode and logs every debug message.} 
influence the number of log messages.
Multiple new instruments were added,
including \texttt{HF2LI} (high--frequency lock--in amplifier, Zurich Instruments), 
\texttt{MFIA} (impedance analyzer, Zurich Instruments), and 
\texttt{LS336} (temperature controller, Lake Shore).
Additionally,
data acquisition functionalities of other instruments were improved.

\subsubsection{Data Acquisition (DAQ) Instruments}
First \marginline{NI PCI--6281} 
data acquisition experiments were performed with a
PCI--6281\index{EVE!NIPCI6281}
(National Instruments, NI)\nomenclature{NI}{National Instruments}
data acquisition card.
It is directly connected to the Peripheral Component Interconnect (PCI)
\nomenclature{PCI}{Peripheral Component Interconnect}
bus connection on the computer 
and provides an application programming interface (API)
\nomenclature{API}{application programming interface}
through the included NI--DAQmx driver.
In previous experiments,
other group members have 
programmed this instrument in EVE
to measure the raw time--signal and
calculate the power spectral density 
(PSD)\nomenclature{PSD}{power spectral density}
\cite{BenediktHartmann.2017,Zielke.2014,AdhamAmayan.2013,SarahOttersbach.2013}.
The PSD is calculated through these digital algorithms
that split, filter, normalize, Fourier transform, and scale
the signal.
I extracted these calculation algorithms from EVE
into a new Python module called \texttt{spectrumanalyzer}
to make it available for re--use.
Additionally,
the plotting library slowed down the PSD calculation.
I solved this issue with a faster plotting algorithm (\texttt{pyqtgraph}).
A second instrument in usage is also capable of recording 
raw time--signals.

\figtop{\textwidth}{daq-time}{fig:daq-time}
		{SR830DAQ Time--stream visualization}
		{The arbitrary points in time $t_0$ to $t_5$
		are used to visualize the
		communication paths between the
		computer and lock--in amplifier SR830
		through the GPIB connection.
	}


The \marginline{SR830DAQ}\label{page:SR830DAQ} 
lock--in SR830 (Stanford Research Systems)
has an internal data buffer storage.
This permits measuring the data at a sampling rate of
$62.5\;\mathrm{mHz} \leq f_S \leq 512\;\mathrm{Hz}$.
I programmed the corresponding DAQ functionality for
the EVE instrument \texttt{SR830}
to access and utilize this data buffer
via the GPIB\nomenclature{GPIB}{General Purpose Interface Bus (IEEE--488)}
connection.
This new SR830DAQ\index{EVE!SR830DAQ}
function provides three different acquisition modes,
FAST, SINGLE, and LOOP.
Figure~\ref{fig:daq-time}
shows a time--stream visualization.
On the right side is an arbitrary time--axis
to illustrate the passing time.
Triggered by the start button, at the time $t_0$, the computer sends a command to the lock--in,
which starts the DAQ measurement.
The lock--in then waits a short time 
before storing the incoming signal in the buffer at time $t_1$.
The data limit is defined by the GPIB protocol, 
which allows storing up to $16\,384$ ($2^{14}$) values in the buffer.
When using the FAST mode,
the lock--in automatically sends a large datastream (blue) to the computer
at all times $t_2$ between $t_1$ and $t_3$.
SR830DAQ FAST mode reads these values simultaneously 
and stores them in a local \texttt{pandas.DataFrame} until stopped.
When using SINGLE mode (teal), the lock--in waits a pre--defined time after $t_1$ and reads out all values in one batch.
The third mode, LOOP, repeats multiple SINGLE modes until stopped.
At some time $t_3$,
when the GPIB buffer can not save more data,
the lock--in is unable to store more data
and consequentially stops the measurement.
The orange area indicates the stored data.
If some mode wants to read the buffered values at time $t_4$ after the measurement stopped,
it receives a timeout error (red) and stops automatically.
The only way to get the measurement starting again is to send a reset and start signal at a time $t_5$,
which deletes the buffer to restart the measurement at $t_1$.
The algorithm does this in the LOOP mode after each SINGLE measurement
and the FAST mode after gathering $15\,000$ points.
Tests have shown that the LOOP mode loses data points during the long data batch’s readout and reset (see description in EVE wiki available via supplemental information in Appendix~\ref{ch:supplement}).


\subsubsection{Data Documentation}\label{sec:data-doc}

All \marginline{Workflow context}
methods mentioned above gather and store the data on a local machine.
These data are neither linked nor do they provide any context yet.
The experimenter can contextualize these data,
ideally providing version--control and metadata.
All performed measurements shown in this thesis are documented
with parameters, notes, and plots in a OneNote notebook.
For more accessible context, the measured data has been version--controlled and augmented with metadata.\index{Data!Workflow!Metadata}
Each filename consists of the measurement number together with
basic parameter settings.
These pieces of information are accessible through regular expressions (Regex).\nomenclature{Regex}{A regular expression defines a search pattern.}
A Python script extracts the information and saves it into a csv file.
This step is relevant for findability and reusability.

An \marginline{Supplemental information} evolving effort for better documentation has led
towards an open--source driven and markdown based
workflow that can convert documentation and metadata
into various presentable formats.
Additional supplemental information on the data and code 
has been made available via a web--page.
This web--page provides more context for better interoperability.
Unfortunately, OneNote does not support an open data format or easy export of the content, and it converts every mathematical formula into a picture.
The notebooks have been made accessible by converting them into
ReStructured Text (rst) files using Pandoc
\cite{Pandoc.2020},
a universal document converter.
The source code and acquired data 
are documented as well as possible,
spending a reasonable amount of time (see Appendix~\ref{ch:supplement}).


\section{Data Analysis}\label{sec:data-analyze}

This open data approach is consequentially
succeeded by the usage of free software for data analysis.
The benefit of free software is 
the independent verifiability of results
by reviewers.
Therefore,
the whole data processing environment is
based solely on free algorithms,
and all self--written Python source codes are released as free software.


\subsection{Spectrumanalyzer}\label{sec:data-spectrum}

As \marginline{First spectrum}
mentioned above, the 
\texttt{spectrumanalyzer}\index{spectrumanalyzer}
Python module has been extracted from the EVE instrument \texttt{NIPCI6281}.
The \texttt{SpectrumAnalyzer} class\nomenclature{Python class}{»A template for creating user--defined objects. Class definitions normally contain method definitions which operate on instances of the class.«
\cite{Python.glossary}} contains all algorithms needed to calculate and save a PSD from a time--signal by means of a 
fast Fourier transform\index{spectrumanalyzer!FFT} 
(FFT).\nomenclature{FFT}{Fast Fourier Transform}
This time--signal $V (t)$ is provided as a single list of values
to create a \texttt{SpectrumAnalyzer} object.
Together with additional required parameters, 
like the sampling rate $f_s$ or the number of first spectra $N$,
this time--signal is then processed by the generator\nomenclature{Python generator}{»A function which returns a generator iterator.«
\cite{Python.glossary}} \texttt{cut\_timesignal}.
This generator, as the name suggests,
splits the time--signal into equidistant shorter signals $V_{(n)} (t)$,
where $n = 1,2,\ldots , N$.
Each of these short signals is filtered
using the \texttt{scipy.signal.butter} filter.
If requested,
the algorithm can downsample the signal,
keeping every $k^{\mathrm{th}}$ point ($k \in \mathds{N}$)
\cite{AdhamAmayan.2013}.
Because the SR830DAQ function measures with low sampling rates 
(compared to the NI PCI--6281),
this downsampling is a drawback and had been bypassed.
The remaining signal then disposes of the mean value,
leaving only fluctuations behind.
These fluctuations are multiplied by a Hanning window (\texttt{numpy.hanning}),
Fourier transformed with \texttt{numpy.fft.fft},
and squared.
A factor of ${2}\cdot {(3/8 \cdot \ell \cdot f_s)^{-1}}$,
where $\ell$ represents the signal’s length,
counters the Hanning window’s effect on the calculated 
FFT amplitude
\cite[pp. 30--35]{Thyzel.2020}.
Finally, the generator yields the calculated PSD $S_V^{(n)} (f)$
for each signal part via an 
iterator.\nomenclature{Python iterator}{»An 
	object representing a stream of data. Repeated calls to the iterator’s \texttt{\_\_next\_\_()} method [...] return successive items in the stream.«
	\cite{Python.glossary}}
This iterator can be used in a \texttt{for} loop to access each intermediate PSD and the resulting final PSD 
$S_V (f) = \frac{1}{N} \sum_{n=1}^N S_V^{(n)} (f) $.
This resulting final PSD is called the first spectrum\index{spectrumanalyzer!first spectrum}.

The \marginline{Second spectrum}%
above described first spectrum does not always contain
all information about the intrinsic noise.
In such cases,
higher--order power spectra are needed.
Here, the second spectrum\index{spectrumanalyzer!second spectrum}
is calculated
\cite{Weissman.1993}
through the
temporal development of the time--resolved first spectrum $S_V^{(n)} (f)$ (see Fig.~\ref{fig:daq-info-446} bottom left plot).
The
integral of the first spectrum
$ P_{(n)} (f_a, f_b) = \int_{f_a}^{f_b} S_V^{(n)} (f) df $
defines the
power inside a pre--defined frequency octave $O_k = [f_a, f_b)$.
The second spectrum $S^{(2)}_{O_k} (f)$ is then derived through the application of the FFT on $P_{(n)} (f_a, f_b)$.
This second spectrum essentially gives rise to the PSD’s fluctuations inside a given octave $O_k$.
The newly developed \texttt{ana} package uses these described algorithms from the \texttt{spectrumanalyzer} module to analyze the data. 

\subsection{Data Analysis Framework (\texttt{ana})}\label{sec:data-ana}

\texttt{ana}\index{ana} \marginline{Functional specifications} is an object--oriented Python framework
to analyze measurements. %
\texttt{ana} has created all figures in Chapter~\ref{ch:results}.
Figure~\ref{fig:ana-uml} shows ana’s classes and their relationship to each other. 
Inherited classes, which share most variables and functions, 
are connected by solid arrows.
In contrast, dashed arrows indicate 
the instancing of a class into an object,
enabling execution of class-internal functions.
\texttt{ana} grants access to single measurements via the \texttt{SingleM} and inherited classes.
These single measurements are customized to a specific instrument and data structure.
Additionally, they incorporate algorithms to read, analyze, and visualize measurements.
The high--level API (top) takes advantage of creating multiple single measurement objects.
This approach allows the combination of various measurements in a single plot.
Figure~\ref{fig:ana-tree} shows this dividing approach 
to access the high--level visualization API through Jupyterlab,
while computing analysis is based on robust open--source algorithms inside the low--level backend.
More detailed technical specifications are available in the supplemental information.

\figtop{\textwidth}{ana-short}{fig:ana-uml}{UML Class--Diagram of \texttt{ana}}{This diagram displays the internal relationship between \texttt{ana}’s classes using the unified modeling language (UML).\nomenclature{UML}{unified modeling language}  Solid arrows indicate a direct inheritance of classes, and dashed arrows show where objects are instanced. \texttt{ana} provides a high--level interface to classes that can handle multiple single measurements (shown in the middle) at once. All measurements classes provide additional access to a low--level python backend, which is used by the high--level interface.}

\figx{\textwidth}{ana-tree}{fig:ana-tree}{Hierarchical functional specifications of \texttt{ana}}{
	This tree shows the external interaction possibilities with \texttt{ana}.
	The low--level python backend (right) is based on robust open--source algorithms that link to state--of--the--art C++ libraries.
	The high--level visualization API allows the interactive exploration of data using widgets.}


The \marginline{\texttt{HLoop}}\index{ana!\texttt{Hloop}}
\texttt{HLoop} class handles lock--in measurements during a sweeping magnetic field.
It requires EVE’s output file for a specific ordered combination of instruments.
This output file contains all information about the measured temperature, magnetic field, and voltages at discrete times during the measurement.
\texttt{HLoop} distinguishes between parallel and gradiometry measurements (see Section~\ref{sec:gradiometry}).
The magnetic stray--field calculation
assumes a constant
current of $I = 2.5\,\mathrm{\mu{}A}$
and electron density $n_e$,
\marginline{$n_e = \frac{I \cdot B_{ext}}{e\cdot V_H} \approx 1.369 \cdot 10^{11} \frac{1}{cm^2}$\\$e$: electron charge}
calculated from the Hall voltage $V_H$ at 
an external magnetic field of
$B_{ext} = 1\,\mathrm{T}$
applied perpendicular to the Hall sensor’s surface
($\theta = 0^{\circ}$, see Fig.~\ref{fig:3d-sensor}).
When the external field is applied in an angle $\theta = 90^{\circ}$,
parallel to the Hall sensor’s surface,
three different Hall crosses are measured simultaneously.
In these parallel measurements, an empty cross signal is subtracted from the two other measured crosses.
This approach eliminates the linear and non--linear Hall background from the sensor
\cite{Pohlit.2017}.
In contrast, gradiometry measurements contain a small
linear background
that is determined with linear regression using \texttt{scipy.stats.linregress} on the negative saturated region.
The remaining non--linear background can be eliminated through the difference between two different sweeps in the same field.
The discrete
down--sweep signal $B^{down} (H_{ext})$ 
and up--sweep signal $B^{up} (H_{ext})$
are interpolated with
\texttt{scipy.interpolate.interp1d}.
The difference plot subtracts these curves
$\Delta B_z (H_{ext}) = \langle B^{down}(H_{ext}) \rangle - \langle B^{up}(H_{ext}) \rangle $.
In this process, information loss is unavoidable.


Measurements \marginline{Fluctuations}
from the signal--analyzer (SR785)\index{EVE!SR785} 
are usually two--dimensional
(see Section~\ref{sec:mag-fluct}),
providing the frequency $f$ and PSD $S_V (f) \sim f^{-\alpha}$.
The analysis in the \texttt{SA} class 
is reduced to a logarithmic linear regression $\log{y} = \alpha \log{x} + x_0$
for a given frequency range.
In this linear regression, 
$x$ and $y$ denote ${f}$ and ${S_V}$,
respectively.
The regression range is restricted by default to
$20\,\mathrm{mHz} < f < 700\,\mathrm{mHz}$
to avoid fitting unrelated artefacts.
This regression outputs the PSD’s slope $\alpha$ and amplitude $x_0 = S_V (f = 1\,\mathrm{Hz})$.
\texttt{RAW}\index{ana!\texttt{RAW}} measurements contain the whole time--signal $V (t)$,
processed through the \texttt{spectrumanalyzer} module described above.


A special \marginline{\texttt{MFN}}
role in the analysis is the combination 
of various single measurements.
The \texttt{MFN}\index{ana!\texttt{MFN}} class was created to handle the measurements presented in Section~\ref{sec:res-sr},
specifically time--signals during an interrupted field sweep.
This class manages multiple \texttt{RAW} measurements
and analyses automated SR830DAQ\index{EVE!SR830DAQ} routines.
These routines create several single measurement files
with varying parameters.
Essential parameters, like position inside the hysteresis,
are encoded into each filename. 
\texttt{MFN}’s constructor function
that initializes each instantiation
scans all filenames with regular expressions
for such parameters and stores them into an \texttt{MFN.info}
dictionary.
Afterward,
each time--signal is processed by the \texttt{ana.RAW} class,
calculating the first spectrum and time--resolved PSDs.
Several plotting functions grant 
visualization of diverse perspectives to the data.
A significant plotting function is the \texttt{MFN.plot\_info()} function
that is used to create the analysis overview in Section~\ref{sec:res-sr}.

\subsection{Jupyter Notebooks}\label{sec:data-jupyter}

\texttt{ana} provides the necessary algorithms to analyze and plot the data.
These algorithms are then utilized and combined
inside Jupyter notebooks.\index{ana!Jupyter}
Jupyter serves a powerful 
integrated development environment (IDE)\nomenclature{IDE}{integrated development environment}
to write and execute Python code inside a web--browser.
Jupyter notebooks are a quick and easy way 
to navigate data and create plots
\cite{ChristianWinkler.16.12.2020,ChristianWinkler.20.1.2021}.
The underlying Python kernel facilitates 
iterative data exploration,
enabling to focus on application logic
rather than implementation details
\cite{MarcusHanhart.16.12.2020}.

\section{Computational Workflow}\label{sec:data-workflow}

\figdoubletop{.65\textwidth}%
			{CI-workflow}%
			{CI Workflow}%
			{.95\textwidth}%
			{infrastructure}%
			{Continuous Analysis Infrastructure}%
			{fig:CI}{Continuous Analysis}{a) Schematic view of the Continuous Integration workflow and created outputs. %
	b) Depiction of changes in the infrastructure. %
	Currently, all files are saved and accessed through local network folders. %
	Continuous Analysis centralizes data and requirements on the GitLab and Docker registry servers, making data, code, and dependencies easily accessible. %
	Code execution and data analysis can be performed on any computer
	using the corresponding Docker image. %
	Inspired by Beaulieu--Jones and Greene \cite{BeaulieuJones.2017}.}

The \marginline{Testing}
code described above can read, analyze, and visualize the data.
Test cases can additionally
improve the stability and reproducibility of critical code segments
\cite{Dubois.2012}.
I extended \texttt{ana} and the \texttt{spectrumanalyzer} with test cases
that ensure the code stability.
The test cases for \texttt{ana} also create and save all plots shown in this thesis.
The code has been tested on 64bit (x64) Windows and Linux, 
as well as on arm architectures, 
like the Raspberry Pi 3 and the new Apple Silicon chip.
These tests can also be performed automatically by GitLab CI/CD\index{GitLab!Continuous Integration (CI)}
\cite{Arefeen.2019}.
Figure~\ref{fig:CI-workflow} shows this CI workflow as a flowchart diagram.
First, a Docker container needs to be created using a Dockerfile.
This container is then uploaded to a registry server,
where it is available for other users.
The GitLab CI configuration needs to include this Docker container
together with instructions.
When this configuration file is in place,
GitLab CI automatically runs the defined scripts
on every push of changed source code or data
and creates a pre--defined output.

\subsection{Continuous Analysis}\label{sec:data-tools}

The \marginline{Version control}
past decades have seen an enormous change in the software development landscape.
Nowadays, it is a well--established philosophy
to take advantage of a version management system
to track differences and history of changes 
and support more extensive collaboration.
The currently used infrastructure (see the top of Figure~\ref{fig:infrastructure}),
where every computer accesses a public folder 
in the local network,
has already proven efficient and successful over the years.
Nevertheless, recent developments of novel free software
facilitate improving this infrastructure
and increasing productivity and collaboration.
GitLab provides secure version control of all projects
and automatically analyses the data with Continuous Analysis.
\index{GitLab!Continuous Analysis}
Multiple tutorials are currently created online
\cite{Carpentries.GitHub,Carpentries.Conda,Carpentries.Docker}
and published in journals
\cite{Wiegenstein.21.10.2020,ChristianRostThorstenSchifferdeckerChristianSchneemann.20.11.2019,Friedrich.20.3.2019,Hecht.26.11.2018,ChristophPuppe.19.4.2017,Baker.2017}
to teach scientists programming skills, as well as
source--code maintenance and testing
with git and CI tools.

Listing~\ref{lst:ci} \marginline{GitLab CI}
shows an annotated excerpt of the used GitLab CI configuration.
A GitLab runner,
executed as a permanent service on the analysis PC,
downloads the git repository from the server
and executes scripts as configured.
Specific variables, like \linebreak%
\texttt{GIT\_SUBMODULE\_STRATEGY}, influence the behavior of the runner.
Stages consist of one or more jobs that run in a given order.
Line 10 defines the used Docker image for a virtual environment.
\index{Docker}
This Docker image contains all requirements needed to run the code.
Line 12 names the current job and all following indented lines!
If some packages or requirements are still missing, a \texttt{before\_script} section executes commands to install them.
This installs the local packages \texttt{spectrumanalyzer}, \texttt{ana}, and EVE.
Afterward, the \texttt{script} section runs all tests in the given \texttt{TEST\_DIR}.
This variable is changed in lines 36 and 41 to execute the tests from a different test directory \texttt{TEST\_DIR}.
The output files of the scripts can be uploaded to the GitLab server as artifacts.
The \texttt{artifacts} section starting in line 26
configures file--patterns that
point to files that are 
uploaded after the execution.
Optional security scans in lines 43ff. 
use pre--configured GitLab CI security templates.
These can extract additional information about used dependencies and license compliances.
The computational environment is defined by the used
Docker image in line 10.
This image can be created with a self--written Dockerfile.

Listing~\ref{lst:docker} \marginline{Dockerfile}
shows an excerpt of the 
Dockerfile
used to create the Docker image.
It is based on a pre--configured Python 3.8 image.
Lines~3ff. download and install the required software 
to create the documentation and \LaTeX{} plots.
Lines~13ff. define needed Python packages for the Python package manager \texttt{pip}.
At the end of the file, an optional \texttt{entrypoint.sh} shell script
is copied to the image to start Jupyterlab.
Jupyterlab provides a webserver to program Jupyter notebooks easily.
This server is not needed when combining the image with GitLab CI.
Therefore, the \texttt{ENTRYPOINT} in line~32 has been disabled 
in the utilized image \texttt{jupyterlab:ana}.
Typically, each processor architecture (x64 or arm) needs a 
specially designed Docker image.
The Docker image presented here is optimized for x64 processors.
In order to create a similar Docker image for arm processors,
the pandoc installation would need a specialized arm version
in line~11.

This Docker image creates custom Docker containers to 
automate code testing with GitLab CI.
On every updated change of the git repository,
GitLab CI automatically runs pre--defined tests
and provides various details after execution.


\clearpage
\begin{lstlisting}[caption={\textbf{\texttt{ana}’s GitLab CI Configuration} (gitlab-ci.yml excerpt)},label={lst:ci},style=yml,escapechar=!]
variables:
  GIT_SUBMODULE_STRATEGY: recursive

stages:
  - doc
  - spectrumanalyzer
  - ana
  - test

image: registry.gitlab.com/ganymede/jupyterlab:ana!\tikz[remember picture] \node [] (image) {};!

ana:prepair:
  stage: ana
  variables:
    TEST_DIR: tests/ana/prepair
  before_script:!\tikz[remember picture] \node [] (before-script) {};!
    - cd spectrumanalyzer && python -m pip install -e . && cd ..
    - cd ana && python -m pip install -e . && cd ..
    - cd EVE && python -m pip install -e . && cd ..
    - python -m pip install coverage
    - cd ana
    - mkdir output
  script:
    - coverage run -m unittest discover -s $TEST_DIR!\tikz[remember picture] \node [] (script) {};!
    - coverage report -m
  artifacts:!\tikz[remember picture] \node [] (artifacts) {};!
    paths:
      - output
  only:
    - master
    - develop

ana:fit:
  extends: ana:prepair
  variables:
    TEST_DIR: tests/ana/fit!\tikz[remember picture] \node [] (test-dir) {};!

ana:visualize:
  extends: ana:prepair
  variables:
    TEST_DIR: tests/ana/visualize

sast:!\tikz[remember picture] \node [] (sast) {};!
  stage: test
  include:
    - template: Security/SAST.gitlab-ci.yml
    - template: Security/Dependency-Scanning.gitlab-ci.yml
    - template: Security/License-Scanning.gitlab-ci.yml
\end{lstlisting}%
\begin{tikzpicture}[remember picture, overlay,
	every edge/.append style = { ->, thick, >=stealth,
		dashed, line width = 1pt, draw = ocre!80},
	every node/.append style = { align = center, minimum height = 10pt,
		font = \bfseries, fill= ocre!20},
	%text width = 4cm 
	]
	\node [above left = .75cm and -.75 cm of image,text width = 2.2cm]
	(A) {Docker image};
	\node [right = 6.cm of before-script,anchor=south]  (B) {install packages};
	\node [right = .6cm of script] (C) {run tests};
	\node [right = 3.cm of artifacts] (ART) {upload created files};
	\node [right = 2.cm of test-dir]  (TEST) {repeat with \\different test dir};
	\node [right = 7.cm of sast]  (S) {security scans};  
	\draw (A.south) edge (A.south |- image.north);
	\draw (S.west) edge (sast.east) ;
	\draw (ART.west) edge (artifacts.east) ;
	\draw (TEST.west) edge (test-dir.east) ;
	\draw (B.west |- before-script.east) 
		edge (before-script.east) ;  
	\draw (C.west) edge (script.east) ;
\end{tikzpicture} 

\clearpage
\begin{lstlisting}[caption={\textbf{Dockerfile Jupyterlab} (excerpt). \\Available at \url{https://gitlab.com/ganymede/jupyterlab}.},label={lst:docker},style=docker,escapechar=!]
FROM python:3.8!\tikz[remember picture] \node [] (from) {};!

RUN curl -sL https://deb.nodesource.com/setup_12.x | bash - && \
	apt-get upgrade -y && \
	apt-get install -y nodejs \!\tikz[remember picture] \node [] (apt) {};!
		texlive texlive-science \
		texlive-latex-extra texlive-xetex \
		dvipng man-db cm-super graphviz && \
	rm -rf /var/lib/apt/lists/*

RUN wget https://github.com/jgm/pandoc/releases/download/2.10.1/pandoc-2.10.1-1-amd64.deb && dpkg -i pandoc-2.10.1-1-amd64.deb && rm -f pandoc-2.10.1-1-amd64.deb

RUN python -m pip install --upgrade pip && \
	python -m pip install --use-feature=2020-resolver --upgrade \
		jupyterlab \!\tikz[remember picture] \node [] (pip) {};!
		sphinx \
		numpy \
		scipy \
		matplotlib \
		pandas \
		seaborn &&\
	jupyter labextension install \
		@jupyter-widgets/jupyterlab-manager \
		@jupyterlab/toc 

COPY bin/entrypoint.sh /usr/local/bin/!\tikz[remember picture] \node [] (copy) {};!
COPY config/ /root/.jupyter/

EXPOSE 8888
VOLUME /notebooks
WORKDIR /notebooks
ENTRYPOINT ["entrypoint.sh"]!\tikz[remember picture] \node [] (entry) {};!
\end{lstlisting}
\begin{tikzpicture}[remember picture, overlay,
	every edge/.append style = { ->, thick, >=stealth,
		dashed, line width = 1pt, draw = ocre!80},
	every node/.append style = { align = center, minimum height = 10pt,
		font = \bfseries, fill= ocre!20},
	%text width = 4cm 
	]
	\node [right = 1 cm of from,
			text width=8cm]
	(from-node) {re-use preconfigured Python image};
%	\node [right = 6.cm of before-script,anchor=south]  (B) {install packages};
	\node [right = .8cm of apt] (apt-node) {install software};
	\node [below = 1.6cm of pip,
			text width = 3cm,
			anchor = west] (pip-node) {install Python packages};
	\node [right = .8cm of copy] (copy-node) {copy files};
	\node [right = .8cm of entry] (entry-node) {install startup script};
%	\node [right = 2.cm of test-dir]  (TEST) {repeat with \\different test dir};
%	\node [right = 7.cm of sast]  (S) {security scans};  
	\draw (from-node.west) edge (from.west) ;
	\draw (apt-node.west) edge (apt.west) ;
	\draw (copy-node.west) edge (copy.west) ;
	\draw (entry-node.west) edge (entry.west) ;
	\draw (pip |- pip-node.north) edge (pip.north) ;
%	\draw (S.west) edge (sast.east) ;
%	\draw (ART.west) edge (artifacts.east) ;
%	\draw (TEST.west) edge (test-dir.east) ;
%	\draw (B.west |- before-script.east) 
%	edge (before-script.east) ;  
%	\draw (C.west) edge (script.east) ;
\end{tikzpicture} 
\clearpage
