\chapter{Magnetic Characterization Techniques}\label{ch:exp}
\vspace{-8mm}
\begin{cBox}[frametitle={Brief Summary}]
	\begin{itemize}
		\item 3D nano--tetrapods are deposited directly on top of the sensor’s active area using \textbf{focused electron beam induced deposition} (FEBID).
		\item \textbf{Micro--Hall magnetometry} provides a versatile tool for magnetic stray--field and fluctuation measurements.
		\item \textbf{Fluctuation spectroscopy} extracts information hidden within noise.
		\item A novel method measures \textbf{magnetic flux noise} (MFN)
		by combining micro--Hall magnetometry and fluctuation spectroscopy.
	\end{itemize}
\end{cBox}

A Hall sensor
is a highly sensitive magnetometer to extract magnetic stray--fields.
In the present study, I utilize a custom made Hall sensor for low temperature and microscale measurements.
This sensor measures the
magnetic signal of three--dimensional nano--tetrapods
and explores
the magnetic characteristics for dynamic conditions.
An automated measurement approach allows 
high--resolution access to a signal.
This high--resolution access facilitates the
	analysis and decomposition of 
	electric and magnetic noise.
By analyzing the electric and magnetic noise, 
	I investigate
	the magnetic characteristics of 
	newly developed nano--tetrapods.

\section{3D Nano--Tetrapods}

\figtop{\textwidth}{nanostructures}{fig:nanostructures}{Hall Sensor with 3D Nano--Tetrapods}{%
	Scanning electron micro\-scopy (SEM) %
	image of the micro--Hall sensor with nano--tetrapods deposited on top. %
	The tetrapods are arranged in $2\times 2$ arrays $2\,\mu$m apart centered in the Hall sensor’s active area. %
	This area is highlighted in blue (\textbf{Crosses}) and red (\textbf{Plusses}). %
	The insets reveal the three--dimensional projection to clarify the arms’ relative directions %
	(Images by Fabrizio Porrati).}

The
\marginline{Geometry}%
examined structures are deposited on top of the
Hall sensor’s top gate 
in collaboration with
the group of Prof. Michael Huth (Goethe University Frankfurt)
using
FEBID\index{focused electron beam induced deposition (\textit{FEBID})}.\nomenclature{FEBID}{focused electron beam induced deposition}
The structures
are designed as ferromagnetic $\mathrm{CoFe}$ tetrapods,\index{Tetrapods}
a single lattice element from the diamond structure discussed in the introduction.
The magnetic moments in such systems can be frustrated
\cite{Isoda.2014,Bramwell.2001}.
The tetrapods are placed as $2\times 2$ arrays in the center of one of the hall bar’s active areas.
Figure~\ref{fig:nanostructures} depicts the used micro--Hall sensor after the deposition of the nano--tetrapods.
Because of the tetrapod's characteristic shape from this perspective,
the blue (left) and red (right) colored structures are named Crosses and Plusses, respectively.
All eight single element tetrapods are identical, differing only in the relative orientation of the external field.
The Plusses and Crosses are rotated by $45^\circ$,
which leads to complex inter--element interactions.
The structure's size was chosen for its relatively small 
and simple geometry.
The array consists of four structures per cross.
This counters expected difficulties in obtaining
a large enough magnetic stray--field
from the structures.

\section{Experiment}\label{sec:exp}
\subsection{Hall Sensor}\label{sec:sensor}

A \marginline{Sensor}
schematic view of the sensor 
is shown in Figure \ref{fig:3d-sensor}.
The teal--colored, two--dimensional electron gas
(2DEG)\index{two-dimensional electron gas (2DEG)}\nomenclature{2DEG}{two-dimensional electron gas}
is located
about $70\,$nm below the 
golden top gate.
Multiple differently doped
$\mathrm{Al_{x}Ga_{1-x}As}$ \index{AlGaAs}
layers
are above and below the 2DEG (not displayed in the figure).
Such micro--Hall sensors are created
with an enormous investment
of workforce and specialized instruments
\cite{Lalehdashti.2016}.
The employed sensor was fabricated by Merlin Pohlit
\cite{Pohlit.2017}
and characterized by Mohanad Al Mamoori
\cite{AlMamoori.2020}.


\figtop{\textwidth}{3d-sensor}{fig:3d-sensor}{Geometric sketch of the used Hall sensor}{%
	The gold top gate and conducting two--dimensional electron gas (2DEG) are painted in yellow and teal, respectively. %
	The 2DEG is approximately $70\,$nm below the top gate %
	and accessible through the numbered connection points. %
	The sensors’s active area highlighted red %
	is positioned in the 2DEG %
	and has a size of %
	$3 \times 3\,\mathrm{\mu{}m^2}$. %
	The nano--tetrapods are deposited on top of the top gate centered in the middle of the active area. %
	The Hall sensor is rotate--able around the y--axis, %
	thus enabling the application of %
	an external magnetic field $\vec{H}_{ext}$ at varying angles $\theta$.}

\figdouble{.49\textwidth}
{cryo}{Cryostat and IVC}
{.46\textwidth}
{instruments}{Instruments}
{fig:inst}
{Photographs of the experimental setup}{%
	(a) Cryostat (right) with the opened inner vacuum chamber (IVC, left).
	Insets are showing a zoomed--in view of the rotate--able sample holder (bottom) and Hall sensor.
	(b) Used instruments for magnetic fluctuation measurements (magnetic power supply and temperature controller not shown).
	The IVC connection box (yellow) is connected to the 2DEG through the
	sample holder.}

The \marginline{Top gate grounding}
sensor’s top gate 
serves as a substrate for the nanostructures and
allows manipulation of the electron density of the 2DEG.
This manipulation reduces avoidable noise sources during the measurement
\cite{Li.2004}.
The Hall sensor’s signal--to--noise ratio 
($\mathrm{SNR} \sim \sqrt{N/\gamma_H}$)
depends on 
the material--specific Hooge--parameter $\gamma_H$,
and the number of electrons $N = A \cdot n_e$ 
inside the active area,
%This number $N$ is
which depend on the area of the
Hall crosses $A$ 
and the electron density $n_e$.
The top gate and an unused Hall bar connection 
are connected to the ground in order to 
avoid potential fluctuations.
This grounding is done by the 
current and voltage source Yokogawa 7651
(see Fig. \ref{fig:instruments}, purple, top right corner).

\figtop{\textwidth}{gradiometry-box}{fig:gradiometry}{Gradiometry Setup}{%
	A schematic representation of the experimental setup is drawn as an electric circuit diagram.
	The external magnetic field $\vec{H}_{ext}$ is applied at an angle $\theta$ in the x--z--plane.
	The gradiometry box (green) splits the voltage into two currents $I_1$ and $I_2$.
	These are sent in opposite directions to cancel the effect of the external field in--situ.
	The resistors $R_1$ and $R_2$ balance the currents while
	the capacitors $C_1$ and $C_2$ eliminate possible phase differences.
	The Hall voltage $V_H$ is measured perpendicular to the currents,
	here along the x--axis.
	The top gate and one vacant Hall cross are grounded in order to suppress potential fluctuations.
}

\subsection{Micro--Hall Magnetometry}\label{sec:gradiometry}

\index{Micro-Hall magnetometry}

All 
\marginline{Cryostat}
experiments were performed inside a 
helium--4 bath cryostat by Janis Research.
Figure \ref{fig:cryo} shows a photograph of the cryostat (right).
The cryostat is equipped with 
a superconducting magnet and 
an inner vacuum chamber (IVC, left).\nomenclature{IVC}{inner vacuum chamber}
Inside this IVC, 
the cryostat provides wiring to
a heater and
a sample holder underneath the top--most sensor (inset bottom).
The sample holder 
can be rotated around one axis
at an angle $\theta$ of circa $-120^{\circ} \lesssim \theta \lesssim 120^{\circ}$.
A small amount of helium exchange gas
couples the sample holder thermally 
to the surrounding He bath.
The heater manipulates 
the temperature during the measurements and is commonly
operated by a proportional--integral--derivative (PID)
\nomenclature{PID}{proportional-integral-derivative}
temperature controller.
The present study uses
%	the corresponding EVE modules for
the temperature controller LS340 (Lake Shore) and 
the magnet’s intelligent power supply IPS120 (Stanford Research Systems)
to control the heater and superconducting magnet,
respectively.


An \marginline{Hall effect}
external magnetic field $\vec{H}_{ext}$,
driven
through the cryostat’s superconducting magnet,
changes the magnetization
of a placed sample.
The magnetized sample creates
a local magnetic stray--field,
which superposes the external field.
This local magnetic stray--field usually 
allows the observation of a hysteresis loop --- the ferromagnetic fingerprint \cite{Bertotti.2008}.
Hysteresis loops usually depend on the temporal history of the
external magnetic field.
The resulting Hall voltage
$V_H = ({n_e \cdot e})^{-1} \langle B_z \rangle$ 
($e$: elementary charge)
is proportional to 
the averaged magnetic stray--field
perpendicular to the sensor’s surface
$\langle B_z \rangle$
(here: z--direction).
The influence of the external field
on the resulting signal
depends on the applied angle 
${\langle B_z \rangle = |\mu_0 \vec{H}_{ext}| \cos \theta}$.
In practice, this allows for a precise rotation of the sensor.
After determination of 
the maximum signal $V_{max} = V_H (\theta = 0^{\circ})$
for a stationary field,
the desired signal can be calculated 
for any new angle 
$V_H (\theta_{new}) = V_{max} \cdot \cos(\theta_{new})$.
During the rotation, capacitive and thermal effects
can influence the result and add an error of approximately $\Delta \theta \approx \pm\; 2-5^{\circ}$.

Figure~\ref{fig:instruments}
\marginline{Experiment}\label{par:experiment}
displays 
a photograph of the instrument rack
highlighting the used instruments.
The IVC connection box (yellow) 
provides wiring to access the sensor 
inside the cryostat.
The default measurement setup
for all angles $\theta \neq \pm 90^{\circ}$
is shown in Figure~\ref{fig:gradiometry}.
The lock--in amplifier SR830 (blue, Stanford Research Systems) 
generates an oscillating voltage $V_{out}$
and measures the signal.
The 
gradiometry box (green) 
splits the oscillating voltage
into two separate signals.
Here, 
transformers $T_1$ and $T_2$ split the signal
before
the conductors $C_1$ and $C_2$ 
and resistors $R_1$ and $R_2$ 
are modulating the phase and
adjust the resulting current $I_x = V_{out} / R_{x}$ (where $x\in \{1,2\}$),
respectively.
This process results in 
two currents $I_1$ and $I_2$,
that flow into opposite directions,
for example connecting
$I_1$ ($13^+$---$1^-$)
and $I_2$ ($6^+$---$8^-$) 
to measure the Plusses 
with the gradiometry technique
(see Fig.~\ref{fig:3d-sensor}).
The second current $I_2$,
which flows through an empty cross,
creates an opposite Hall voltage
annulling this superposed signal.
The resulting Hall voltage $V_H$,
measured at $7^+$---$14^-$,
is reduced by a factor of up to $10^3$.
The gradiometry technique\index{Micro-Hall magnetometry!Gradiometry}
allows the cancellation of 
the external magnetic field signal
in--situ.
This in--situ cancellation
was derived from the gravity gradiometry technique
\cite{Bell.1998}.
The gradiometry technique 
improves the sensitivity significantly
and provides
a powerful tool when facing
large external signals.


As \marginline{Background}
mentioned above,
the gradiometry technique is 
well suited to remove the background
for angles $\theta \neq \pm 90^{\circ}$.
After the in--situ subtraction of the external magnetic field, a linear and non--linear background remains.
A linear regression at negative saturation determines the linear part.
Determining the non--linear part is more challenging and transgresses the scope of this study.
When the external field is applied parallel to the sensor’s surface ($\theta = \pm 90^{\circ}$),
the external field superposes no significant Hall voltage.
These angles allow 
sending a single current along with the long bar 
$7^+$---$14^-$
and measuring all three 
Hall signals simultaneously (in parallel).
In contrast to the gradiometry technique, this is called the parallel technique\index{Micro-Hall magnetometry!Parallel}.
The small background signal is then obtained from the empty cross.
All fitted curves presented in the results are retrieved by subtracting the above--described background.
Additionally, a difference plot also eliminates the non--linear background. However, eliminating this non--linear background causes information loss.
The difference plot represents the width of the magnetic hysteresis loop at a given field by subtracting them
$\Delta B_z (H_{ext}) = \langle B^{down}(H_{ext}) \rangle - \langle B^{up}(H_{ext}) \rangle $,
where $\langle B^{down}(H_{ext}) \rangle$ and $\langle B^{up}(H_{ext}) \rangle$ are the calculated measured stray--fields during the down--and up--sweep of the magnetic field, respectively.
The area under the $\Delta B_z$ curve is equal to the area of the hysteresis loop.
In practice, with varying discrete measurement values for $H_{ext}$, it is necessary to interpolate before performing the subtraction.
These background fitting methods are programmed into \texttt{ana.Hloop} (see Section~\ref{sec:data-ana}).


\subsection{Magnetic Fluctuations}\label{sec:mag-fluct}

The \marginline{Electric noise}
above--described background subtraction methods
are changing the signal only marginally
and, therefore, preserve vital information.
This information is extracted 
through the resulting hysteresis loops
and difference plots.
The aforementioned high temporal resolution enables the analysis
to additionally measure electric noise.
This noise is amplified 
before it is processed by the lock--in,
using a pre--amplifier, 
like the SR560 (Stanford Research Systems, see Fig.~\ref{fig:instruments}, top left corner, teal).
After both the pre--amplifier and lock--in
amplify and filter the signal,
it is relayed for post--processing.

Two \marginline{SR785 and SR830DAQ}
post--processing techniques are compared.
First, the signal is relayed to the
signal--analyzer 
SR785 (Stanford Research Systems, see Fig.~\ref{fig:instruments}, bottom, orange).\index{EVE!SR785}
This instrument automatically 
applies the fast Fourier transformation (FFT) multiple times, 
averages it, 
and outputs the power spectral density (PSD) in the frequency domain.
As a result of this process, the original time--signal is not accessible anymore.
This absent signal is usually not burdensome
when the emanating process
is stationary, in 
place, and ergodic in time.
In such a case, the signal follows the normal distribution,
and the PSD represents the signal’s entire noise.
In contrast,
the previous statement does not apply to
dynamic processes,
where a signal changes the mean value over time
and loses its ergodicity.
In aforesaid dynamic processes,
higher--order power spectra 
(second spectrum\index{spectrumanalyzer!second spectrum}) are necessary
to determine the significant noise 
characteristics.
Those noise characteristics use the temporal development of the time--resolved PSD (see Section~\ref{sec:data-spectrum}).
This information is accessible through the original time--signal
measured by the lock--in’s internal buffer.
The corresponding EVE instrument function \texttt{SR830DAQ} (see Section~\ref{sec:data-ana}, Page~\pageref{page:SR830DAQ})\index{EVE!SR830DAQ} 
is capable of performing automated routines\index{EVE!routine},
such as writing a list of commands that programs multiple instruments at once.

The \marginline{Magnetic noise}
combination of electric noise and
magnetic characterization measurements
opens exciting windows of opportunities.
To seize these opportunities,
I examined the magnetic noise
under various conditions.
The Hall bar’s and structures’ static noise is obtained 
by selecting and preparing stationary positions inside the hysteresis loop.
The opportunity
to measure long time--signals automated through EVE’s routines
allows the preparation of many equidistant positions inside the
hysteresis.
This approach generates an immense amount of data
that is needed for the investigation of low--frequency dynamics.
Additionally,
various conditions
during a changing external magnetic field
are explored.
