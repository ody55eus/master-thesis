\chapter{Supplemental Information} \label{ch:supplement}
\vspace{-18mm}
\section{Data, Code and Documentation}
%
\begin{wrapfigure}{r}{.48\linewidth}
	\includegraphics[width=\linewidth]{file-tree}
	\caption[Project folder organization]{\label{fig:file-tree}
		\textbf{Project folder organization}.}
\end{wrapfigure}
	Supplemental information about data and code are available in the Lab--Book.
	The Lab--Book’s structure is shown in Figure~\ref{fig:lab-book}.
	Source code and data documentation is mainly written by experimenters and coders to be helpful for their future--self and later adapted for reviewers, spending a reasonable amount of time.

	Figure~\ref{fig:file-tree} shows the file structure of the Lab--Book’s repository.
	Documentation and analysis scripts are in subfolders 
	\texttt{docs/} and \texttt{docs/notebooks/}, respectively.
	The original OneNote Notebook exports are available inside the
	\texttt{source/onenote/} folder.
	Test cases to recreate the plots from Chapter~\ref{ch:results}
	can be found in \texttt{ana}’s test folder\footnote{\texttt{ana.git/tests/ana/visualize/test\_master\_plots.py}}.
\clearpage
\begin{tBox}[frametitle={Note}]
	Data, code, and documentation is available for reviewers.
	The supplemental information is provided through a self--maintained server on the internet,
	and available for the review period:
	
	\url{https://master.ody5.de}
\end{tBox}

\figx{\textwidth}{lab-book}{fig:lab-book}{Structure of supplemental information in the Lab Book}{}


\chapter{Legal notices}\label{ch:legal}

Figure~\ref{fig:licenses} depicts an overview of common open--source licenses.\index{Licensing}
All documentation regarding the data, including created graphs, pictures, diagrams, and context, can be licensed using Creative Commons licenses
\cite{CreativeCommons.Share}.
Source code for computer programs and scripts should take advantage of the number of open--source licenses
\cite{GitHub.Appendix}
to protect the work from unwanted infringements or other legal problems.

\figx{\textwidth}{licenses}{fig:licenses}{Open Source Licenses}{A selection of popular open source licenses ordered from most permissive (left) to restricted (right).}

\figtop{\textwidth}{EVE-packages}{fig:EVE-packages}{Python dependencies of EVE and spectrumanalyzer and resulting license compliances}{}

The programmed software builds on open--source modules.
The usage of these modules requires compliance with the agreed license.
One such requirement involves for derived works to include the copyright owner and license of the software.
Figure~\ref{fig:EVE-packages} shows the software dependencies of EVE and the spectrumanalyzer and the corresponding licenses.
Table~\ref{tab:licenses} and~\ref{tab:licenses2} shows the tools that are involved in the creation of this thesis.

\begin{table}[htb]
	\begin{center}
		\begin{tabular}{lr|lr}
			Name & Version & Copyright & License\\ \hline
			Python & 3.8 & Python Software Foundation & PSF Python 3.9.0 \\
			EVE & ebdb829 & AGM (B. Hartmann et al.) & \\
			spectrumanalyzer & 3e30865 & AGM (A. Amayan et al.) & \\
			{ana} & 7923164 & Jonathan Pieper & GNU GPLv3 \cite{GNU.GPLv3.Overview} \\
			matplotlib & 3.3.3 & Paul Barrett \cite{Barrett.2004} & matplotlib\\
			seaborn & 0.11.1 & Michael L. Waskom & BSD (3--Clause) \cite{BSD-3.Overview} \\
			numpy & 1.19.5 & Stefan van der Walt \cite{vanderWalt.2011} & BSD (3--Clause) \cite{BSD-3.Overview} \\
			SciPy & 1.6.0 & Pauli Virtanen \cite{Virtanen.2020} & BSD (3--Clause) \cite{BSD-3.Overview} \\
			Pandas & 1.2.0 & NumFOCUS & BSD (3--Clause) \cite{BSD-3.Overview} \\
			h5py & 3.1.0 & The HDF Group & Apache 2.0 \cite{Apache.License} \\
			pyqtgraph & 0.12.2 & Luke Campagnola & MIT \cite{MIT.Overview} \\
			PyVisa & 1.11.3 & PyVISA Authors & MIT \cite{MIT.Overview} \\
			NI-Visa & 20.0 & National Instruments & NI EULA\nomenclature{EULA}{end user license agreement} \\
			NI-DAQmx & 20.0 & National Instruments & NI EULA \\
			nidaqmx & 0.5.7 & National Instruments & MIT \cite{MIT.Overview} \\
			PyQT5 & 5.15.2 & Riverbank Computing & GNU GPLv3 \cite{GNU.GPLv3.Overview} \\ \hline
		\end{tabular}
	\end{center}
	\caption{Software and licenses connected to research study.}
	\label{tab:licenses}
\end{table}

\begin{table}[htb]
	\begin{center}
		\begin{tabular}{lr|lr}
			Name & Version & Copyright & License\\ \hline
			Ubuntu & 18.04 & Cannonical Ltd. & GNU GPLv3 \cite{GNU.GPLv3.Overview} \\
			GitLab & 13.x.x & GitLab B.V. & MIT
			\cite{MIT.Overview} \\
			Gitlab--Runner & 13.x.x & GitLab B.V. & MIT
			\cite{MIT.Overview} \\
			Docker Desktop & 20.10.8 & Docker & Apache 2.0 \cite{Apache.License} \\
            Jupyterlab Image & 763e2154 & Jonathan Pieper & MIT \cite{MIT.Overview} \\
			Jupyter Core & 4.7.0 & Project Jupyter & BSD (3--Clause) \cite{BSD-3.Overview} \\
			Jupyter Lab & 3.0.5 & Project Jupyter & BSD (3--Clause) \cite{BSD-3.Overview} \\
			Anaconda &  & Anaconda Inc. \cite{Anaconda.hp} & BSD (3--Clause) \cite{BSD-3.Overview} \\
			Pandoc & 2.10.1 & \cite{Pandoc.2020} & BSD (3--Clause) \cite{BSD-3.Overview} \\
			Sphinx & 3.4.3  & \cite{Brandl.2007} & BSD (3--Clause) \cite{BSD-3.Overview} \\
			IPython & 7.19.0 & Fernando Pérez \cite{Perez.2007} & BSD (3--Clause) \cite{BSD-3.Overview} \\
			coverage & 5.3.1 & Ned Batchelder & Apache 2.0 \cite{Apache.License} \\
			SciencePlots & ac8d772 & John Garrett \cite{Garrett.2020} & MIT \cite{MIT.Overview} \\
		\end{tabular}
	\end{center}
	\caption{Additional software and licensed used in continuous analysis.}
	\label{tab:licenses2}
\end{table}


\section{License and Copyright notices}

\paragraph{Thesis Content and \LaTeX Code}
This thesis is published under the terms of the Creative Commons Attribution 4.0 International (\textbf{CC BY} 4.0) license
\cite{CC.BY}.
Permission to the \LaTeX{} source code is granted under condition of the \textbf{MIT} license
\cite{OpenSource.MIT}.
The \texttt{structure.tex} file is adapted from »The Legrand Orange Book«\footnote{\url{https://www.latextemplates.com/template/the-legrand-orange-book}} by Mathias Legrand\footnote{legrand.mathias@gmail.com} with modifications by Vel\footnote{vel@latextemplates.com} and published under the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported (\textbf{CC BY-NC-SA} 3.0) license \cite{CC.BY-NC-SA}.

\paragraph{Python Data Analysis Framework (Ana) and Jupyter Notebooks}
Copyright \textcopyright{} 2020/2021 Jonathan Pieper

»This program is free software: you can redistribute it and/or modify
it under the terms of the \textbf{GNU General Public License} as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. 
If not, see \url{https://www.gnu.org/licenses/}.« \cite{GNU.GPLv3.Overview}

\paragraph{Lab--Book}
The Lab--Book as a compendium of all its containing code, data, documentation, and examples is a database by definition of the German »Urheberrecht«
\cite[§87a]{Urheberrecht.gesetz}. 
Everyone is welcome to redistribute it and/or modify it under the terms of the Creative Commons Attribution 4.0 International (\textbf{CC BY} 4.0 \cite{CC.BY}) license.

\chapter{Open--Source Licenses}
\vspace{18mm}
The following open--source licenses are part of the most popular and permissive licenses available.

\section{MIT License}\label{sec:mit}
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

\section{BSD (3--Clause) License}
Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

\begin{enumerate}
	\item Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
	
	\item Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
	
	\item Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
\end{enumerate}

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


\input{gpl}
